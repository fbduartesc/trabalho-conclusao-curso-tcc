<?php
class Ponto_model extends Model
{
	function Ponto_model()
	{
		parent::Model();
	}
	
	function add($ponto)
	{
		if (empty($ponto['cd_at_equipamento_porta']))
		{
			$ponto['cd_at_equipamento_porta'] = NULL;
		}
		
		$this->db->insert('ponto', array('nm_ponto'                => $ponto['nm_ponto'],
										 'ds_local'                => $ponto['ds_local'],
										 'cd_bloco'                => $ponto['cd_bloco'],
										 'cd_at_equipamento_porta' => $ponto['cd_at_equipamento_porta'],
										 'cd_tipo_ponto'           => $ponto['cd_tipo_ponto'],
										 'ds_posicao'              => $ponto['ds_posicao'],
		                                 'in_status'               => $ponto['in_status']));
	}
	
    function edit($ponto)
    {
    	if (empty($ponto['cd_at_equipamento_porta']))
        {
            $ponto['cd_at_equipamento_porta'] = NULL;
        }
        
        $this->db->where('cd_ponto', $ponto['cd_ponto']); 
        $this->db->update('ponto', array('nm_ponto'                => $ponto['nm_ponto'],
                                         'ds_local'                => $ponto['ds_local'],
                                         'cd_bloco'                => $ponto['cd_bloco'],
                                         'cd_at_equipamento_porta' => $ponto['cd_at_equipamento_porta'],
                                         'cd_tipo_ponto'           => $ponto['cd_tipo_ponto'],
                                         'ds_posicao'              => $ponto['ds_posicao'],
                                         'in_status'               => $ponto['in_status']));
    }
    
    function remove($ds_posicao)
    {
        $sql = "DELETE
                    FROM ponto
                    WHERE ds_posicao = '{$ds_posicao}'
                    LIMIT 1";
                    
        $query = $this->db->query($sql);        
    }
    
    function get_all()
    {
        $sql = 'SELECT ds_posicao
                    FROM ponto';
        
        $query = $this->db->query($sql);
        
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum Ponto encontrado');
        }
        else 
        {
            return $query->result();
        }               
    }
	
    function get_point_by_position($ds_posicao)
    {       
        $sql = "SELECT p.*, ae.cd_equipamento, ae.cd_at, ae.cd_at_equipamento, pe.*
				  FROM ponto p
				    LEFT JOIN at_equipamento_porta pe ON pe.cd_at_equipamento_porta = p.cd_at_equipamento_porta
				    LEFT JOIN at_equipamento ae ON ae.cd_at_equipamento = pe.cd_at_equipamento
				  WHERE p.ds_posicao = '{$ds_posicao}'";
                          
        $query = $this->db->query($sql);

        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum ponto encontrado');
        }
        else 
        {
            return $query->row();
        }
    }
	
	
	
	
	
	/* VERIFICAR AS FUNCOES ABAIXO */
	function busca_dados_edit($id)
	{
		$sql = "SELECT * FROM ponto
				WHERE cd_ponto = ".$id;
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function busca_posicao($posicao)
	{
		$sql = "SELECT cd_ponto, ds_posicao FROM ponto WHERE ds_posicao = '".$posicao. "'" ;
		$query = $this->db->query($sql);
		return $query->row();		
	}
	
	function busca_ponto_editar($id)
	{
		$sql = "SELECT * FROM ponto
				WHERE cd_ponto = ".$id."";
		$query = $this->db->query($sql);
		return $query->row();
	}
}

/* End of file ponto_model.php */
/* Location: ./system/application/models/ponto_model.php */