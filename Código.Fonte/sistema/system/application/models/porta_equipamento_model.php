<?php
class Porta_equipamento_model extends Model
{
	function Porta_equipamento_model()
	{
		parent::Model();
	}
	
	function add($cd_at_equipamento, $nr_porta, $in_status)
	{	
        $this->db->insert('at_equipamento_porta', array('cd_at_equipamento' => $cd_at_equipamento,
                                                        'nr_porta'          => $nr_porta,
                                                        'in_status'         => $in_status));	
	}
	
	function update_status_by_code($cd_at_equipamento_porta, $in_status)
	{
		$this->db->where('cd_at_equipamento_porta', $cd_at_equipamento_porta); 
        $this->db->update('at_equipamento_porta', array('in_status' => $in_status));
	}
    
    function remove_doors_of_at_equipment($cd_at_equipamento)
    {
        $sql = "DELETE
                    FROM at_equipamento_porta
                    WHERE cd_at_equipamento = {$cd_at_equipamento}";
                    
        $query = $this->db->query($sql); 
    }
    	
	function remove_doors_of_points($cd_at_equipamento)
	{
        $sql = "UPDATE ponto
                    SET cd_at_equipamento_porta = NULL
                    WHERE cd_at_equipamento_porta IN (SELECT cd_at_equipamento_porta
                                                        FROM at_equipamento_porta
                                                        WHERE cd_at_equipamento = {$cd_at_equipamento})";
                    
        $query = $this->db->query($sql); 
	}
	
    function remove_door_of_point($cd_at_equipamento_porta)
    {
        $sql = "UPDATE ponto
                    SET cd_at_equipamento_porta = NULL
                    WHERE cd_at_equipamento_porta = {$cd_at_equipamento_porta}";
                    
        $query = $this->db->query($sql); 
    }
	
	function get_all_by_status($cd_at_equipamento, $in_status)
	{
		$sql = "SELECT *
		          FROM at_equipamento_porta
				  WHERE cd_at_equipamento = {$cd_at_equipamento}
				        AND in_status = '{$in_status}'
				  ORDER BY nr_porta";
		
		$query = $this->db->query($sql);
		
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhuma porta dispon�vel com a situa��o informada');
        }
        else 
        {
            return $query->result();
        }
	}
	
    function get_all_by_at_code($cd_at)
    {
        $sql = "SELECT at.cd_equipamento, at.cd_at_equipamento, at.nr_posicao,
                       p.nr_porta, p.in_status, p.is_certificado, p.cd_at_equipamento_porta,
                       a.nr_tamanho, e.nm_equipamento, e.ds_modelo
                    FROM at_equipamento at, at_equipamento_porta p, at a, equipamento e
                    WHERE at.cd_at_equipamento = p.cd_at_equipamento
                          AND at.cd_equipamento = e.cd_equipamento
                          AND a.cd_at = at.cd_at
                          AND at.cd_at = {$cd_at}
                    ORDER BY at.nr_posicao ASC";
        
        $query = $this->db->query($sql);
        
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum equipamento relacionado a este AT');
        }
        else 
        {
            return $query->result();
        }
    }
}

/* End of file porta_equipamento_model.php */
/* Location: ./system/application/models/porta_equipamento_model.php */