<?php
class At_equipamento_model extends Model
{
	function At_equipamento_model()
	{
		parent::Model();
	}
	
    function add($at_equipamento)
    {
        $this->db->insert('at_equipamento', array('cd_at'          => $at_equipamento['cd_at'],
			                                      'cd_equipamento' => $at_equipamento['cd_equipamento'],
			                                      'nr_posicao'     => $at_equipamento['nr_posicao'],
			                                      'ds_serie'       => $at_equipamento['ds_serie'],
			                                      'in_status'      => $at_equipamento['in_status']));
        
        return $this->db->insert_id();
    }
    
    function edit($at_equipamento)
    {
        $this->db->where('cd_at_equipamento', $at_equipamento['cd_at_equipamento']);
        $this->db->update('at_equipamento', array('cd_at'          => $at_equipamento['cd_at'],
                                                  'cd_equipamento' => $at_equipamento['cd_equipamento'],
                                                  'nr_posicao'     => $at_equipamento['nr_posicao'],
                                                  'ds_serie'       => $at_equipamento['ds_serie'],
                                                  'in_status'      => $at_equipamento['in_status']));
    }
    
    function remove($cd_at_equipamento)
    {
        $sql = "DELETE
                    FROM at_equipamento
                    WHERE cd_at_equipamento = {$cd_at_equipamento}";
                    
        $query = $this->db->query($sql);        
    }
    
    function get_by_code($cd_at_equipamento)
    {
        $sql = "SELECT *
                    FROM at_equipamento
                    WHERE cd_at_equipamento = {$cd_at_equipamento}";
        
        $query = $this->db->query($sql);
        
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum At-Equipamento encontrado');
        }
        else 
        {
            return $query->row();
        }        
    }
    
    function get_all_by_at_code($cd_at)
    {
        $sql = "SELECT *
                    FROM at_equipamento at
                    WHERE at.cd_at = {$cd_at}
                    ORDER BY at.nr_posicao ASC";
        
        $query = $this->db->query($sql);
        
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum equipamento relacionado a este AT');
        }
        else 
        {
            return $query->result();
        }
    }
    
    /*ver m�todos abaixo*/
	function verifica_posicao($posicao, $id_at)
	{
		$sql = "SELECT * FROM at_equipamentos
				WHERE at_id = ". $id_at."
				AND posicao = ".$posicao;
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
			return true;
		else
			return false;
	}
	
	function listagem_todos()
	{
		$sql = "SELECT * FROM at_equipamentos ORDER BY at_id";
		$query = $this->db->query($sql);
		return $query->result();		
	}
	
    function ajax_get_all_by_at_code($cd_at)
    {
        $sql = "SELECT a.cd_at_equipamento, e.ds_modelo, e.nr_porta, e.nm_equipamento 
                    FROM at_equipamento a, equipamento e
                    WHERE a.cd_at = {$cd_at}
                          AND e.cd_equipamento = a.cd_equipamento
                    ORDER BY e.nm_equipamento";
                    
        $query = $this->db->query($sql);
        
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum equipamento relacionado a este AT');
        }
        else 
        {
            return $query->result();
        }
    }
}
?>
