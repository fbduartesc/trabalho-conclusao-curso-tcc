<?php
class Point_type_model extends Model
{
	function Point_type_model()
	{
		parent::Model();
	}

	
	function get_all()
	{
		$sql = 'SELECT *
		          FROM tipo_ponto
		          ORDER BY nm_tipo_ponto';
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
}

/* End of file type_point_model.php */
/* Location: ./system/application/models/type_point_model.php */