<?php
class Bloco_model extends Model
{
	function Bloco_model()
	{
		parent::Model();
	}

	function get_all()
	{
		$sql = 'SELECT *
		          FROM bloco
		          ORDER BY nm_bloco';
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
}

/* End of file bloco_model.php */
/* Location: ./system/application/models/bloco_model.php */