<?php
class Report_model extends Model
{   
    function Report_model()
    {
        parent::Model();
    }
    
    function list_all_at()
    {
    	$sql = 'SELECT b.nm_bloco, a.nm_at, a.ds_local
					FROM at a, bloco b
					WHERE a.cd_bloco = b.cd_bloco
					ORDER BY b.nm_bloco, a.nm_at';
    	
        $query = $this->db->query($sql);
          
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum AT encontrado');
        }
        else 
        {
            return $query->result();
        }
    }
    
    function list_all_at_equipment()
    {
        $sql = 'SELECT b.nm_bloco, a.cd_at, a.nm_at, a.ds_local,
				       e.nm_equipamento, e.ds_modelo,  ae.cd_at_equipamento, ae.in_status,
				       COUNT(*) AS total_portas, aep.in_status AS in_status_porta
                    FROM at a, bloco b, at_equipamento ae, equipamento e, at_equipamento_porta aep
                    WHERE a.cd_bloco = b.cd_bloco
                          AND a.cd_at = ae.cd_at
                          AND e.cd_equipamento = ae.cd_equipamento
                          AND aep.cd_at_equipamento = ae.cd_at_equipamento
                    GROUP BY aep.cd_at_equipamento, aep.in_status
                    ORDER BY b.nm_bloco, a.nm_at, e.nm_equipamento, aep.in_status';
        
        $query = $this->db->query($sql);
          
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum Equipamento encontrado');
        }
        else 
        {
            return $query->result();
        }
    }
    
    function list_all_point($in_status)
    {
        $sql = "SELECT b.nm_bloco, a.cd_at, a.nm_at, p.nm_ponto, p.ds_local, tp.nm_tipo_ponto
					FROM ponto p, tipo_ponto tp, at_equipamento_porta aep, at_equipamento ae, at a, bloco b
					WHERE p.cd_tipo_ponto = tp.cd_tipo_ponto
					      AND p.cd_at_equipamento_porta = aep.cd_at_equipamento_porta
					      AND aep.cd_at_equipamento = ae.cd_at_equipamento
					      AND ae.cd_at = a.cd_at
					      AND a.cd_bloco = b.cd_bloco
					      AND p.in_status = '{$in_status}'
					ORDER BY b.nm_bloco, a.nm_at, p.nm_ponto";
        
        $query = $this->db->query($sql);
          
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum Ponto encontrado');
        }
        else 
        {
            return $query->result();
        }
    }
    
    function list_all_door($in_status)
    {
        $sql = "SELECT b.nm_bloco, a.cd_at, a.nm_at, e.nm_equipamento, e.ds_modelo, aep.nr_porta
					FROM at_equipamento_porta aep, at_equipamento ae, at a, equipamento e, bloco b
					WHERE aep.cd_at_equipamento = ae.cd_at_equipamento
					      AND ae.cd_at = a.cd_at
					      AND ae.cd_equipamento = e.cd_equipamento
					      AND a.cd_bloco = b.cd_bloco
					      AND aep.in_status = '{$in_status}'
					ORDER BY b.nm_bloco, a.nm_at, ae.nr_posicao, e.nm_equipamento, aep.nr_porta";
        
        $query = $this->db->query($sql);
          
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhuma Porta encontrado');
        }
        else 
        {
            return $query->result();
        }
    }
}

/* End of file report_model.php */
/* Location: ./system/application/models/report_model.php */