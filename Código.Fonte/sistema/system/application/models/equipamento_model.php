<?php
class Equipamento_model extends Model
{
	function Equipamento_model()
	{
		parent::Model();
	}
	
    function get_by_code($cd_equipamento)
    {
        $sql = "SELECT *
                    FROM equipamento
                    WHERE cd_equipamento = {$cd_equipamento}";
        
        $query = $this->db->query($sql);
        
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum Equipamento encontrado');
        }
        else 
        {
            return $query->row();
        }        
    }
	
	function get_all()
	{
        $sql = 'SELECT *
                  FROM equipamento
                  ORDER BY nm_equipamento, ds_modelo';
        
        $query = $this->db->query($sql);
        
        return $query->result();    	
	}
}
?>
