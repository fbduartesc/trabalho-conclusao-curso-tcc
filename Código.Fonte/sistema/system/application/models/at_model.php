<?php
class At_model extends Model
{	
	function At_model()
	{
		parent::Model();
	}
	
	function add($at)
	{
        $this->db->insert('at', array('cd_bloco'         => $at['cd_bloco'],
                                      'nm_at'            => $at['nm_at'],
                                      'ds_local'         => $at['ds_local'],
                                      'nr_tamanho'       => $at['nr_tamanho'],
                                      'ds_posicao'       => $at['ds_posicao'],
                                      'nr_identificacao' => $at['nr_identificacao']));
	}
	
    function edit($at)
    {
        $this->db->where('cd_at', $at['cd_at']);
        $this->db->update('at', array('cd_bloco'         => $at['cd_bloco'],
                                      'nm_at'            => $at['nm_at'],
                                      'ds_local'         => $at['ds_local'],
                                      'nr_tamanho'       => $at['nr_tamanho'],
                                      'ds_posicao'       => $at['ds_posicao'],
                                      'nr_identificacao' => $at['nr_identificacao']));
    }
    
    function remove($ds_posicao)
    {
        $sql = "DELETE
                    FROM at
                    WHERE ds_posicao = '{$ds_posicao}'
                    LIMIT 1";
                    
        $query = $this->db->query($sql);        
    }
	
    function get_at_by_position($ds_posicao)
    {
        $sql = "SELECT *
                    FROM at
                    WHERE ds_posicao = '{$ds_posicao}'";
        
        $query = $this->db->query($sql);
        
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum AT encontrado');
        }
        else 
        {
            return $query->row();
        }
    }
    
    function get_all()
    {
        $sql = 'SELECT *
                  FROM at
                  ORDER BY nm_at';
        
        $query = $this->db->query($sql);
          
        if($query->num_rows() == 0) 
        {
            throw new Exception('Nenhum AT encontrado');
        }
        else 
        {
            return $query->result();
        }
    }
    
    /*VERIFICAR AS FUN��ES ABAIXO*/
	function busca_at($id)
	{
		$sql = "SELECT * FROM at WHERE cd_at = ".$id;
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function listagem_todos()
	{
		$sql = "SELECT * FROM at ORDER BY nm_at";
		$query = $this->db->query($sql);
		return $query->result();		
	}
	
	function busca_posicao($posicao)
	{
		$sql = "SELECT cd_at, ds_posicao FROM at WHERE ds_posicao = '{$posicao}'" ;
		$query = $this->db->query($sql);
		return $query->row();		
	}
	
	function apaga_posicao($posicao)
	{
		$sql = "DELETE FROM at WHERE cd_at = {$posicao} LIMIT 1";
		$query = $this->db->query($sql);
		return $query->result();				
	}
	
	function busca_portas_equipamento($cd_at_equipamento)
	{
		$sql = "SELECT at.cd_at, p.nr_porta, p.in_status, p.is_certificado, p.cd_at_equipamento_porta 
			        FROM at_equipamento at, at_equipamento_porta p
					WHERE at.cd_at_equipamento = p.cd_at_equipamento
					      AND at.cd_at_equipamento = {$cd_at_equipamento}";
					      
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function retorna_situacao($id)
	{
		$sql = "SELECT *
		          FROM at_equipamento_porta
				  WHERE cd_at_equipamento_porta = {$id}";
		$query = $this->db->query($sql);
		return $query->row();
	}
}

/* End of file at_model.php */
/* Location: ./system/application/models/at_model.php */