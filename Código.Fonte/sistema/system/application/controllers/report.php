<?php
class Report extends Controller
{
	public $data; 

    function Report()
    {
        parent::Controller();
        $this->load->library('layout', 'layout_main');
        $this->load->model('Report_model');
        
        $this->data['menu'] = $this->load->view('report/menu', NULL, TRUE);
    }
    
    function index()
    {
    	$this->data['content_report'] = NULL;
    	$this->layout->view('report/main', $this->data);
    }
    
    function at()
    {
    	try
    	{
    		$this->data['ats']            = $this->Report_model->list_all_at();
            $this->data['content_report'] = utf8_decode($this->load->view('report/list_at', $this->data, TRUE));
    	}
    	catch(Exception $e)
    	{
    		$this->data['content_report'] = $e->getMessage();
    	}
        
    	$this->layout->view('report/main', $this->data);
    }
    
    function at_equipment()
    {
    	try
        {
            $this->data['at_equipments']  = $this->Report_model->list_all_at_equipment();
            $this->data['content_report'] = utf8_decode($this->load->view('report/list_at_equipment', $this->data, TRUE));
        }
        catch(Exception $e)
        {
            $this->data['content_report'] = $e->getMessage();
        }
        
        $this->layout->view('report/main', $this->data);
    }
    
    function point($in_status)
    {
    	try
        {
            $this->data['points']         = $this->Report_model->list_all_point($in_status);
            $this->data['content_report'] = utf8_decode($this->load->view('report/list_point', $this->data, TRUE));
        }
        catch(Exception $e)
        {
            $this->data['content_report'] = $e->getMessage();
        }
        
        $this->layout->view('report/main', $this->data);
    }
    
    function door($in_status)
    {
    	try
        {
            $this->data['doors']          = $this->Report_model->list_all_door($in_status);
            $this->data['content_report'] = utf8_decode($this->load->view('report/list_door', $this->data, TRUE));
        }
        catch(Exception $e)
        {
            $this->data['content_report'] = $e->getMessage();
        }
        
        $this->layout->view('report/main', $this->data);
    }
}

/* End of file report.php */
/* Location: ./system/application/controllers/report.php */