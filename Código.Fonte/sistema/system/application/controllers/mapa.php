<?php

class Mapa extends Controller {

	function Mapa()
	{
		parent::Controller();
		$this->load->library('layout', 'layout_main'); 		
	}
	
	function index()
	{
		$this->layout->view('index');			
	}
}

/* End of file mapa.php */
/* Location: ./system/application/controllers/mapa.php */