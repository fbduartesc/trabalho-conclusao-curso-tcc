<?php

class Porta_equipamento extends Controller {

	function Porta_equipamento()
	{
		parent::Controller();
 	
		$this->load->model('Porta_equipamento_model');
	}
	
    function ajax_get_all_porta_equipamento()
    {
        $json['error'] = NULL;
        
        try
        {
            $cd_at_equipamento = $this->input->post('cd_at_equipamento');
            $data['portas']    = $this->Porta_equipamento_model->get_all_by_status($cd_at_equipamento, 'I');
            $json['conteudo']  = utf8_encode($this->load->view('porta_equipamento/get_all_porta_equipamento', $data, TRUE));
        }
        catch(Exception $e)
        {
            $json['error']    = utf8_encode(get_validation_message($e->getMessage(), 'error'));
            $json['conteudo'] = utf8_encode($this->load->view('porta_equipamento/get_all_porta_equipamento', NULL, TRUE));
        }
        
        echo json_encode($json);
        exit();
    }
    
    function ajax_update_status_door()
    {
    	$json['error'] = NULL;
    	
    	try
    	{
    		$cd_at_equipamento_porta = (int) $this->input->post('cd_at_equipamento_porta', TRUE);
    		$in_status               = (string) strtoupper($this->input->post('in_status', TRUE));
    		
            $this->Porta_equipamento_model->update_status_by_code($cd_at_equipamento_porta, $in_status);
            
            if ($in_status == 'I')
            {
            	$this->Porta_equipamento_model->remove_door_of_point($cd_at_equipamento_porta);
            }
            
            $json['color'] = $in_status == 'D' ? 'vermelho' : ($in_status == 'A' ? 'verde' : 'preto');
    	}
    	catch(Exception $e)
    	{
    		$json['error'] = utf8_encode($e->getMessage());
    	}
    	
    	echo json_encode($json);
    	exit();
    }
}

/* End of file porta_equipamento.php */
/* Location: ./system/application/controllers/porta_equipamento.php */