<?php

class At_equipamento extends Controller
{
	function At_equipamento()
	{
		parent::Controller();
		
		$this->load->model('At_equipamento_model');
	}
	
    public function get_form_add()
    {   
        $this->_validate_data();
        
        $data['cd_at'] = (int) $this->input->post('cd_at');
        $json['error'] = NULL;

        try
        {
            $this->load->model(array('Equipamento_model', 'At_model'));
            
            $data['ats']             = $this->At_model->get_all();
            $data['equipamentos']    = $this->Equipamento_model->get_all();
            $data['at_equipamentos'] = $this->_list_all_equipment_door($data['cd_at']);
            $json['conteudo']        = utf8_encode($this->load->view('at_equipamento/add', $data, TRUE));
            
        }
        catch(Exception $e)
        {
            $json['error'] = utf8_encode($e->getMessage());
        }
                        
        echo json_encode($json);
        exit();       
    }
    
    public function get_form_edit()
    {
        $this->_validate_data();
        
        $data['cd_at']             = (int) $this->input->post('cd_at');
        $data['cd_at_equipamento'] = (int) $this->input->post('cd_at_equipamento');
        $json['error']             = NULL;

        try
        {
            $this->load->model(array('Equipamento_model', 'At_model'));
            
            $data['ats']             = $this->At_model->get_all();
            $data['equipamentos']    = $this->Equipamento_model->get_all();
            $data['at_equipamento']  = $this->At_equipamento_model->get_by_code($data['cd_at_equipamento']);
            $data['at_equipamentos'] = $this->_list_all_equipment_door($data['cd_at']);
            $json['conteudo']        = utf8_encode($this->load->view('at_equipamento/edit', $data, TRUE));
            
        }
        catch(Exception $e)
        {
            $json['error'] = utf8_encode($e->getMessage());
        }
                        
        echo json_encode($json);
        exit();       
    }
    
    public function add()
    {
       $json['error'] = NULL;
       
       $this->_validate_data();

       if ($this->validation->run() === FALSE)
       {
           $json['error'] = utf8_encode(get_validation_message($this->validation->error_string, 'error'));
       }
       else
       {
           $cd_at              = (int) $this->_grava_at_equipamento();
           $json['success']    = utf8_encode(get_validation_message('<p>Equipamento cadastrado com sucesso.</p>', 'success'));
           $json['conteudo']   = $this->_list_all_equipment_door($cd_at);
       }
        
       echo json_encode($json);
       exit();
    }
    
    public function edit()
    {
        $json['error'] = NULL;
            
        $this->_validate_data();

        if ($this->validation->run() === FALSE)
        {
            $json['error'] = utf8_encode(get_validation_message($this->validation->error_string, 'error'));
        }
        else
        {
           $cd_at              = (int) $this->_grava_at_equipamento();
           $json['success']    = utf8_encode(get_validation_message('<p>Equipamento atualizado com sucesso.</p>', 'success'));
           $json['conteudo']   = $this->_list_all_equipment_door($cd_at);
        }
        
        echo json_encode($json);
        exit();
    }
    
    public function remove()
    {
        try
        {
            $json['error']     = NULL;
            $cd_at_equipamento = (int) $this->input->post('cd_at_equipamento', TRUE);
            $cd_at             = (int) $this->input->post('cd_at', TRUE);
            
            $this->load->model('Porta_equipamento_model');
            
            $this->Porta_equipamento_model->remove_doors_of_points($cd_at_equipamento);
            $this->Porta_equipamento_model->remove_doors_of_at_equipment($cd_at_equipamento);
            $this->At_equipamento_model->remove($cd_at_equipamento);

            $json['conteudo'] = utf8_encode($this->_list_all_equipment_door($cd_at));
            $json['success']  = 'Equipamento removido com sucesso!';
        }
        catch(Exception $e)
        {
            $json['error'] = utf8_encode($e->getMessage());
        }
        
        echo json_encode($json);
        exit();
    }
    
    public function ajax_get_all_by_at_code()
    {
        $json['error'] = NULL;
        
        try
        {
            $cd_at                = $this->input->post('cd_at');
            $data['equipamentos'] = $this->At_equipamento_model->ajax_get_all_by_at_code($cd_at);
            $json['conteudo']     = utf8_encode($this->load->view('at_equipamento/ajax_get_all', $data, TRUE));
        }
        catch(Exception $e)
        {
            $json['error']    = utf8_encode(get_validation_message($e->getMessage(), 'error'));
            $json['conteudo'] = utf8_encode($this->load->view('at_equipamento/ajax_get_all', NULL, TRUE));
        }
        
        echo json_encode($json);
        exit();
    }
    
    private function _validate_data()
    {
        $this->validation->set_rules(array('cd_equipamento' => 'trim|required',
                                           'cd_at'          => 'trim|required',
                                           'nr_posicao'     => 'trim|required|numeric',
                                           'in_status'      => 'trim|required'));     

        $this->validation->set_fields(array('cd_equipamento' => 'Equipamento',
                                            'cd_at'          => 'At',
                                            'nr_posicao'     => 'Posi��o',
                                            'ds_serie'       => 'S�rie',
                                            'in_status'      => 'Situa��o'));
    }
    
    private function _grava_at_equipamento()
    {
        $at_equipamento                      = NULL;
        $at_equipamento['cd_at_equipamento'] = (int) $this->input->post('cd_at_equipamento', TRUE);
        $at_equipamento['cd_at']             = (int) $this->input->post('cd_at', TRUE);
        $at_equipamento['cd_equipamento']    = (int) $this->input->post('cd_equipamento', TRUE);
        $at_equipamento['nr_posicao']        = (int) $this->input->post('nr_posicao', TRUE);
        $at_equipamento['ds_serie']          = (string) $this->input->post('ds_serie', TRUE);
        $at_equipamento['in_status']         = (string) $this->input->post('in_status', TRUE);
       
        if (empty($at_equipamento['cd_at_equipamento']))
        {
            $cd_at_equipamento = (int) $this->At_equipamento_model->add($at_equipamento);
            
            $this->_grava_at_equipamento_porta($cd_at_equipamento, $at_equipamento['cd_equipamento']);
        }
        else
        {
            $this->At_equipamento_model->edit($at_equipamento);
        }
        
        return $at_equipamento['cd_at'];
    }
    
    private function _grava_at_equipamento_porta($cd_at_equipamento, $cd_equipamento)
    {
    	try
    	{
    		$this->load->model(array('Equipamento_model', 'Porta_equipamento_model'));
    		
    		$nr_porta = (int) $this->Equipamento_model->get_by_code($cd_equipamento)->nr_porta;
            
            if ( ! empty($nr_porta))
            {
                $i = 1;
                while ($i <= $nr_porta)
                {
                    $this->Porta_equipamento_model->add($cd_at_equipamento, $i, 'I');
                    $i++;
                }
            }       
    	}
    	catch(Exception $e)
    	{
    		return $e->getMessage();
    	}
    }
	
    private function _list_all_equipment_door($cd_at)
    {
        try
        {
           $this->load->model('Porta_equipamento_model');
           $data['portas'] = $this->Porta_equipamento_model->get_all_by_at_code($cd_at);
           
           return $this->load->view('porta_equipamento/list_all_equipment_door', $data, TRUE);
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        }
    }
    
    /*refer funcionalidades abaixo*/
	function lista(){
		$this->load->model('At_equipamento_model');
		$query = $this->At_equipamento_model->listagem_todos();
		//$this->layout->view('cabeamento/lista', $data);
		foreach($query as $row):
		echo $row->cd_at_equipamento. "<br />";
		endforeach;
	}
}


/* End of file at_equipamento.php */
/* Location: ./system/application/controllers/at_equipamento.php */