<?php
class Ponto extends Controller
{
	function Ponto()
	{
		parent::Controller();
		$this->load->model('Ponto_model');	
	}
	
    public function get_form_add()
    {
        $this->_validate_data();
        
        $json['error'] = NULL;
       
        try
        {
            $this->load->model(array('Bloco_model', 'At_model', 'Point_type_model'));
            
            $data['bloco']      = $this->Bloco_model->get_all();   
            $data['at']         = $this->At_model->listagem_todos();         
            $data['tipo_ponto'] = $this->Point_type_model->get_all();
            $json['conteudo']   = utf8_encode($this->load->view('ponto/add', $data, TRUE));
        }
        catch(Exception $e)
        {
        	$json['error'] = utf8_encode($e->getMessage());
        }
        
        echo json_encode($json);
        exit();
    }
    
    public function get_form_edit()
    {
        $ds_posicao       = (string) $this->input->post('ds_posicao');
        $json['error'] = NULL;

    
        try
        {
            $this->load->model(array('Bloco_model', 'At_model', 'Point_type_model'));
            
            $data['ponto']      = $this->Ponto_model->get_point_by_position($ds_posicao);
            $data['bloco']      = $this->Bloco_model->get_all();
            $data['tipo_ponto'] = $this->Point_type_model->get_all();
            $data['at']         = $this->At_model->listagem_todos();
        }
        catch(Exception $e)
        {
            $json['error'] = utf8_encode($e->getMessage());
        }

        if (empty($json['error']))
        {
	        try
	        {
	            $this->load->model(array('At_equipamento_model', 'Porta_equipamento_model'));
	            
	            if (! empty($data['ponto']->at_id))
	            {
	               $data['equipamentos'] = $this->At_equipamento_model->get_all_at_equipamento($data['ponto']->at_id);
	            }
	            
	            if (! empty($data['ponto']->cd_at_equipamento))
	            {
	               $data['portas'] = $this->Porta_equipamento_model->get_all_by_status($data['ponto']->cd_at_equipamento, 'I');
	            }
	        }
	        catch(Exception $e){}
        }
        
        $json['conteudo']   = utf8_encode($this->load->view('ponto/edit', $data, TRUE));
        
        echo json_encode($json);
        exit();
    }
	
	public function add()
	{
        $json['error'] = NULL;
        	
		$this->_validate_data();

        if ($this->validation->run() === FALSE)
        {
            $json['error'] = utf8_encode(get_validation_message($this->validation->error_string, 'error'));
        }
        else
        {
            $json['ds_posicao'] = (string) $this->_grava_ponto();
            $json['success']    = utf8_encode(get_validation_message('<p>Ponto cadastrado com sucesso.</p>', 'success'));
            $json['loading']    = utf8_encode(get_validation_message('<p>Aguarde, carregando formul�rio...</p>', 'success'));
        }
        
        echo json_encode($json);
        exit();
	}
	
    public function edit()
    {
        $json['error'] = NULL;
            
        $this->_validate_data();

        if ($this->validation->run() === FALSE)
        {
            $json['error'] = utf8_encode(get_validation_message($this->validation->error_string, 'error'));
        }
        else
        {
            $json['ds_posicao'] = (string) $this->_grava_ponto();
            $json['success']    = utf8_encode(get_validation_message('<p>Dados atualizados com sucesso.</p>', 'success'));
            $json['loading']    = utf8_encode(get_validation_message('<p>Aguarde, carregando formul�rio...</p>', 'success'));
        }
        
        echo json_encode($json);
        exit();
    }
    
    function remove()
    {
        try
        {
            $json['error'] = NULL;
            $ds_posicao    = (string) $this->input->post('ds_posicao', TRUE);
            
            $this->_update_status_at_equipamento_porta($ds_posicao, 'I');
            $this->Ponto_model->remove($ds_posicao);
            
            $json['success'] = 'Ponto removido com sucesso!';
        }
        catch(Exception $e)
        {
            $json['error'] = utf8_encode($e->getMessage());
        }
        
        echo json_encode($json);
        exit();
    }
    
    function list_all()
    {
        try
        {
        	$list   = NULL;
        	$points = $this->Ponto_model->get_all();
        	
            foreach ($points as $i => $row_point)
            {          
                $list.= $row_point->ds_posicao . (isset($points[$i + 1]) ? '|' : NULL);
            }
        }
        catch(Exception $e){}
        
        echo $list;
        exit();
    }
	
	private function _validate_data()
    {
        $this->validation->set_rules(array('nm_ponto'  => 'trim|required',
                                           'ds_local'  => 'trim|required',
                                           'cd_bloco'  => 'trim|required|numeric',
                                           'in_status' => 'trim|required'));    
        
        $this->validation->set_fields(array('nm_ponto'  => 'Nome',
                                            'ds_local'  => 'Local',
                                            'cd_bloco'  => 'Bloco',
                                            'in_status' => 'Situa��o'));
    }
	
    private function _grava_ponto()
    {
        $ponto                            = NULL;
        $ponto['cd_ponto']                = (int) $this->input->post('cd_ponto', TRUE);
        $ponto['nm_ponto']                = (string) $this->input->post('nm_ponto', TRUE);
        $ponto['ds_local']                = (string) $this->input->post('ds_local', TRUE);
        $ponto['cd_bloco']                = (int) $this->input->post('cd_bloco');
        $ponto['cd_at_equipamento_porta'] = (int) $this->input->post('cd_at_equipamento_porta');
        $ponto['cd_tipo_ponto']           = (int) $this->input->post('cd_tipo_ponto');
        $ponto['ds_posicao']              = (string) $this->input->post('ds_posicao', TRUE);
        $ponto['in_status']               = (string) $this->input->post('in_status', TRUE);
       
        if (empty($ponto['cd_ponto']))
        {
            $this->Ponto_model->add($ponto);
        }
        else
        {
            $this->_update_status_at_equipamento_porta($ponto['ds_posicao'], 'I');
            $this->Ponto_model->edit($ponto);
        }
        
        $this->_update_status_at_equipamento_porta($ponto['ds_posicao'], 'A');
        
        return $ponto['ds_posicao'];
    }
    
    private function _update_status_at_equipamento_porta($ds_posicao, $in_status)
    {
    	try
    	{
    	    $cd_at_equipamento_porta = (int) $this->Ponto_model->get_point_by_position($ds_posicao)->cd_at_equipamento_porta;
            
            if ( ! empty($cd_at_equipamento_porta))
            {
                $this->load->model('Porta_equipamento_model');
                $this->Porta_equipamento_model->update_status_by_code($cd_at_equipamento_porta, $in_status);
            }
    	}
    	catch(Exception $e) {}
    }
	
    
    /* VERIFICAR AS FUNCOES ABAIXO */	
	function busca_dados_ponto()
	{			
		$this->load->model('Ponto_model');
		$this->load->library('CarregaPonto');
		$row = $this->Ponto_model->get_point_by_position($this->input->post('ponto'));
		
		echo "<input type='hidden' id='id_ponto' name='id_ponto' value=".$row->id." />".$row->id. "<br />";
		echo "Nome do ponto: ". $row->nome_ponto. "<br />";
		echo "Local do ponto: ". $row->local_sala. "<br />";
		echo "Bloco de localização: ". $row->bloco_ponto. "<br />";
		echo "Tipo de ponto: ". $row->tipo. "<br />";					
		echo "Nome do AT em que está ligado: ". $row->nome_at. "<br />";
		//exit();
	}
	
	function busca_posicao()
	{
		$this->load->model('Ponto_model');
		$query = $this->Ponto_model->busca_posicao($this->input->post('ponto'));
		//return $query;
		echo $query->id;
	}
	
	function busca_dados_edit()
	{
		$this->load->model('Ponto_model');
		$this->load->library('CarregaPonto');
		$query = $this->Ponto_model->busca_dados_edit($this->uri->segment(3));
		//return $query;
		//echo $query->id;
		echo $this->carregaponto->mostra_formulario_edicao($query);
		
		
	}

}

/* End of file ponto.php */
/* Location: ./system/application/controllers/ponto.php */