<?php

class At extends Controller {

	public function At()
	{
		parent::Controller();

		$this->load->model('At_model');
	}
	
    public function get_form_add()
    {   
        $this->_validate_data();
        
        $json['error'] = NULL;

        try
        {
            $this->load->model('Bloco_model');
            $data['blocos']   = $this->Bloco_model->get_all();
            $json['conteudo'] = utf8_encode($this->load->view('at/add', $data, TRUE));
        }
        catch(Exception $e)
        {
        	$json['error'] = utf8_encode($e->getMessage());
        }
                        
        echo json_encode($json);
        exit();       
    }
    
    public function get_form_edit()
    {
        $ds_posicao    = (string) $this->input->post('ds_posicao');
        $json['error'] = NULL;
    
        try
        {
            $this->load->model('Bloco_model');
            
            $data['at']     = $this->At_model->get_at_by_position($ds_posicao);
            $data['blocos'] = $this->Bloco_model->get_all();
        }
        catch(Exception $e)
        {
            $json['error'] = utf8_encode($e->getMessage());
        }
        
        $json['conteudo'] = utf8_encode($this->load->view('at/edit', $data, TRUE));
        
        echo json_encode($json);
        exit();
    }
	
	public function add()
	{
	   $json['error'] = NULL;
	   
	   $this->_validate_data();

	   if ($this->validation->run() === FALSE)
	   {
	       $json['error'] = utf8_encode(get_validation_message($this->validation->error_string, 'error'));
	   }
	   else
	   {
	       $json['ds_posicao'] = (string) $this->_grava_at();
	       $json['success']    = utf8_encode(get_validation_message('<p>Arm�rio de telecomunicacao cadastrado com sucesso.</p>', 'success'));
	       $json['loading']    = utf8_encode(get_validation_message('<p>Aguarde, carregando formul�rio...</p>', 'success'));
	   }
		
	   echo json_encode($json);
	   exit();
	}
	
    public function edit()
    {
        $json['error'] = NULL;
            
        $this->_validate_data();

        if ($this->validation->run() === FALSE)
        {
            $json['error'] = utf8_encode(get_validation_message($this->validation->error_string, 'error'));
        }
        else
        {
            $json['ds_posicao'] = (string) $this->_grava_at();
            $json['success']    = utf8_encode(get_validation_message('<p>Dados atualizados com sucesso.</p>', 'success'));
            $json['loading']    = utf8_encode(get_validation_message('<p>Aguarde, carregando formul�rio...</p>', 'success'));
        }
        
        echo json_encode($json);
        exit();
    }
    
    function remove()
    {
        try
        {
            $json['error'] = NULL;
            $ds_posicao    = (string) $this->input->post('ds_posicao', TRUE);
            $at            = $this->At_model->get_at_by_position($ds_posicao);
            
            try
            {
                $this->load->model(array('At_equipamento_model', 'Porta_equipamento_model'));
                
                $at_equipamentos = $this->At_equipamento_model->get_all_by_at_code($at->cd_at);
                
                foreach ($at_equipamentos as $at_equipamento)
                {
                    $this->Porta_equipamento_model->remove_doors_of_points($at_equipamento->cd_at_equipamento);
                    $this->Porta_equipamento_model->remove_doors_of_at_equipment($at_equipamento->cd_at_equipamento);
                    $this->At_equipamento_model->remove($at_equipamento->cd_at_equipamento);
                }
            }
            catch(Exception $e){}
            
            $this->At_model->remove($ds_posicao);
            
            $json['success'] = 'AT removido com sucesso!';
        }
        catch(Exception $e)
        {
            $json['error'] = utf8_encode($e->getMessage());
        }
        
        echo json_encode($json);
        exit();
    }
    
    function list_all()
    {
    	try
    	{
    		$list = NULL;
            $ats  = $this->At_model->get_all();
                                             
	        foreach ($ats as $i => $row_at)
	        {          
	           $list.= $row_at->ds_posicao . (isset($ats[$i + 1]) ? '|' : NULL);
	        } 
    	}
    	catch(Exception $e) {}
    	
    	echo $list;
    	exit(); 
    }
	
	private function _validate_data()
    {
        $this->validation->set_rules(array('nm_at'      => 'trim|required',
                                           'ds_local'   => 'trim|required',
                                           'nr_tamanho' => 'trim|required|numeric',
                                           'cd_bloco'   => 'trim|required|numeric'));     

        $this->validation->set_fields(array('nm_at'      => 'Nome',
                                            'ds_local'   => 'Local',
                                            'nr_tamanho' => 'tamanho',
                                            'cd_bloco'   => 'bloco'));
    }
	
	private function _grava_at()
    {
        $at                     = NULL;
        $at['cd_at']            = (int) $this->input->post('cd_at', TRUE);
	    $at['nm_at']            = (string) $this->input->post('nm_at', TRUE);
	    $at['ds_local']         = (string) $this->input->post('ds_local', TRUE);
	    $at['nr_tamanho']       = (int) $this->input->post('nr_tamanho', TRUE);
	    $at['cd_bloco']         = (int) $this->input->post('cd_bloco', TRUE);
	    $at['ds_posicao']       = (string) $this->input->post('ds_posicao', TRUE);
	    $at['nr_identificacao'] = (int) $this->input->post('nr_identificacao', TRUE);
	   
	    if (empty($at['cd_at']))
        {
            $this->At_model->add($at);
        }
        else
        {
            $this->At_model->edit($at);
        }
        
        return $at['ds_posicao'];
	}
	
	/**
	* Verificar as fun��es abaixo
	*/
	function lista(){
		$query = $this->At_model->listagem_todos();
		//$this->layout->view('cabeamento/lista', $data);
		foreach($query as $row):
		echo $row->nome. "<br />";
		endforeach;
	}
	
	function busca_posicao()
	{
		$query = $this->At_model->busca_posicao($this->input->post('at'));
		//return $query;
		echo $query->id;
	}
	
	function apaga_posicao()
	{
		$this->At_model->apaga_posicao($this->uri->segment(3));			
	}
	
	function busca_dados_at()
	{		
		$query = $this->At_model->get_at_by_position($this->input->post('at'));
		$tamanho_at = $this->At_model->retorna_tamanho($this->input->post('at'));
		
		echo "<div id='corpo'><ul>";
		echo "</div>";
		
		echo $tamanho_at->tamanho;
		
		foreach($query as $i => $row){
			$id = $row->cd_at_equipamento;				
			if($i == 0){
				echo "<a href='#'>Editar</a><br />";
				echo "Tamanho do at: ". $row->tamanho. "<br />";
				echo "Nome: ". $row->nome. "<br />";
				echo "Local setor: ". $row->local_setor. "<br />";
				echo "Tamanho: ". $row->tamanho. "<br />";
				echo "Bloco: ". $row->bloco. "<br />";					
			}
			echo "<br />";
			/*echo "Equipamento: ". $row->equipamento. "<br />";
			echo "Modelo: ". $row->modelo. "<br />";
			echo "Numero de portas: ". $row->nm_portas. "<br />";*/
			
			$portas = $this->At_model->busca_portas_equipamento($id);
			
			echo "<b>".$row->equipamento. " - ". $row->modelo. " - " .$row->nm_portas. "</b><br />";
			echo "<div id='corpo'><ul>";
			
			foreach($portas as $j => $porta)
			{
				if($porta->situacao == "A")
				{
					$situacao = " class='verde'";
				}else if($porta->situacao == "I")
				{
					$situacao = " class='preto'";
				}else if($porta->situacao == "D")
				{
					$situacao = " class='vermelho'";
				}					
				//echo '<li><a '.$situacao.' id="'.$porta->id_porta.'" href="#" onclick="muda_cor('.$porta->id_porta.', \''.$porta->situacao.'\');" title="Porta '.$porta->numero_porta.'">&nbsp;</a></li>';										
				echo '<li><a '.$situacao.' id="'.$porta->id_porta.'" href="#" onclick="muda_cor('.$porta->id_porta.');" title="Porta '.$porta->numero_porta.'">&nbsp;</a></li>';										
			}
			echo "</ul></div>";
		}			
		
		
		
	}
}

/* End of file at.php */
/* Location: ./system/application/controllers/at.php */