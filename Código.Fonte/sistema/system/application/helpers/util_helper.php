<?php

/**
 * Retorna a mensagem de erro ou sucesso
 *
 * @return string
 */
function get_validation_message($message, $id)
{
    $class_message = array('error'   => 'error_message',
                           'success' => 'success_message');
    
    if ($message == str_replace('<p>', '', $message))
    {
        $message = "<p>{$message}</p>";
    }

    return "<div id=\"{$class_message[$id]}\" onclick=\"$(this).hide();\">{$message}</div>";
}

/* End of file util_helper.php */
/* Location: ./system/application/helpers/util_helper.php */