<div id="formulario">
    <div id="msg_envio_form"></div>
	<form action="<?php echo base_url(); ?>index.php/ponto/add" id="CadPonto" name="CadPonto" method="post" onsubmit="return save_form(this, '#dialog-ponto');" >
    	<fieldset>
            <legend>Ponto - Adicionar</legend>
            
            <input type="hidden" id="ds_posicao" name="ds_posicao" value="" />
			
            <label for="select_at">Selecione o AT:</label>
            <select id="select_at" name="select_at" onchange="get_all_at_equipamento(this);">
                <option value="0">Selecione um AT</option>
                <?php
                    if (isset($at) && is_array($at))
                    {
                        foreach ($at as $row_at)
                        {
                            echo '<option value="' . $row_at->cd_at . '">' . $row_at->nm_at . '</option>';						
                        }
                    }
                ?>	
            </select>
            
            <label for="select_equipamento">Selecione o Equipamento:</label>
            <select id="select_equipamento" name="select_equipamento" onchange="get_all_porta_equipamento(this);">					
	           <option value='0'>Selecione um equipamento</option>					
            </select>

            <label for="cd_at_equipamento_porta">Selecione a porta do equipamento:</label>
            <select id="cd_at_equipamento_porta" name="cd_at_equipamento_porta">					
                <option value=''>Selecione a porta do equipamento</option>										
            </select>

            <label for="bloco">Bloco:</label>
            <select id="cd_bloco" name="cd_bloco">
                <option value="">Selecione um bloco</option>
                <?php
                    if (isset($bloco) && is_array($bloco))
                    {
                        foreach ($bloco as $row_bloco)
                        {
                            echo '<option value="' . $row_bloco->cd_bloco . '">' . utf8_decode($row_bloco->nm_bloco) . '</option>';                      
                        }
                    }
                ?>
            </select>

            <label for="local">Local da sala:</label>
            <input name="ds_local" type="text" id="ds_local" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->ds_local; }?>" />

            <label for="nome">Nome do ponto:</label>
            <input name="nm_ponto" type="text" id="nm_ponto" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->nm_ponto; }?>" />
		
            <label for="cd_tipo_ponto">Tipo de ponto:</label>
            <select id="cd_tipo_ponto" name="cd_tipo_ponto">
                <?php
                    if (isset($tipo_ponto) && is_array($tipo_ponto))
                    {
                        foreach ($tipo_ponto as $row_tipo)
                        {
                            echo '<option value="' . $row_tipo->cd_tipo_ponto . '">' . utf8_decode($row_tipo->nm_tipo_ponto) . '</option>';                      
                        }
                    }
                ?>
            </select>
            
            <label for="in_status">Situa��o:</label>
            <select id="in_status" name="in_status">
                <option value="A">Ativo</option>                      
                <option value="I">Inativo</option>
            </select>
            
            <div class="form_button">
                <input name="adicionar" type="submit" id="adicionar" value="Adicionar"  />
                <input name="limpar" type="reset" id="limpar" value="Limpar" />
            </div>
        </fieldset>	
    </form>
</div>