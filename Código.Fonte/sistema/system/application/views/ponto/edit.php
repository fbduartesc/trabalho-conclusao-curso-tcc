<div id="formulario">
    <div id="msg_envio_form"></div>
    <form action="<?php echo base_url(); ?>index.php/ponto/edit" id="CadPonto" name="CadPonto" method="post" onsubmit="return save_form(this, '#dialog-ponto');" >
        <fieldset>
            <legend>Ponto - Editar</legend>

            <input type="hidden" id="cd_ponto" name="cd_ponto" value="<?php echo $ponto->cd_ponto; ?>" />
            <input type="hidden" id="ds_posicao" name="ds_posicao" value="<?php echo $ponto->ds_posicao; ?>" />
            
            <label for="select_at">Selecione o AT:</label>
            <select id="select_at" name="select_at" onchange="get_all_at_equipamento(this);">
                <option value="0">Selecione um AT</option>
                <?php
                    if (isset($at) && is_array($at))
                    {
                        foreach ($at as $row_at)
                        {
                ?>
                           <option value="<?php echo $row_at->cd_at; ?>" <?php echo $row_at->cd_at == $ponto->cd_at ? 'selected="selected"' : NULL; ?>><?php echo $row_at->nm_at; ?></option>
                <?php
                        }
                    }
                ?>  
            </select>
            
            <label for="select_equipamento">Selecione o Equipamento:</label>
            <select id="select_equipamento" name="select_equipamento" onchange="get_all_porta_equipamento(this);">                  
                <option value='0'>Selecione um equipamento</option>                  
                <?php
                    if (isset($equipamentos) && is_array($equipamentos))
                    {
                        foreach ($equipamentos as $row_equipamento)
                        {
                ?>
                            <option value="<?php echo $row_equipamento->cd_at_equipamento; ?>" <?php echo $row_equipamento->cd_at_equipamento == $ponto->cd_at_equipamento ? 'selected="selected"' : NULL; ?>><?php echo "{$row_equipamento->nm_equipamento} - {$row_equipamento->nr_porta} portas"; ?></option>
                <?php
                        }
                    }
                ?>
            </select>

            <label for="cd_at_equipamento_porta">Selecione a porta do equipamento:</label>
            <select id="cd_at_equipamento_porta" name="cd_at_equipamento_porta">                  
                <option value="">Selecione a porta do equipamento</option>
                <?php
                    if ( ! empty($ponto->cd_at_equipamento_porta))
                    {
                        if ($ponto->in_status == 'I')
                        {
                            $situacao = 'Inativo';
                        }
                        else
                        {
                            $situacao = 'Ativo';
                        }
                             
                        if ($ponto->is_certificado == 'N')
                        {
                            $certificado = 'N�o';
                        }
                        else
                        {
                            $certificado = 'Sim';
                        }
                ?>
                        <option value="<?php echo $ponto->cd_at_equipamento_porta; ?>" selected="selected"><?php echo "{$ponto->nr_porta} - {$situacao} - {$certificado}"; ?></option>
                <?php
                    }
                    
                    if (isset($portas) && is_array($portas))
                    {
                        foreach ($portas as $row_porta)
                        {
                            if ($row_porta->in_status == 'I')
				            {
				               $situacao = 'Inativo';
				            }
				            else
				            {
				               $situacao = 'Ativo';
				            }
				             
				            if ($row_porta->is_certificado == 'N')
				            {
				               $certificado = 'N�o';
				            }
				            else
				            {
				               $certificado = 'Sim';
				            }
                ?>
                            <option value="<?php echo $row_porta->cd_at_equipamento_porta; ?>"><?php echo "{$row_porta->nr_porta} - {$situacao} - {$certificado}"; ?></option>
                <?php
                        }
                    }
                ?>
            </select>

            <label for="cd_bloco">Bloco:</label>
            <select id="cd_bloco" name="cd_bloco">
                <option value="">Selecione um bloco</option>
                <?php
                    if (isset($bloco) && is_array($bloco))
                    {
                        foreach ($bloco as $row_bloco)
                        {
                ?>
                            <option value="<?php echo $row_bloco->cd_bloco; ?>" <?php echo $row_bloco->cd_bloco == $ponto->cd_bloco ? 'selected="selected"' : NULL; ?>><?php echo utf8_decode($row_bloco->nm_bloco); ?></option>
                <?php
                        }
                    }
                ?>
            </select>

            <label for="ds_local">Local da sala:</label>
            <input name="ds_local" type="text" id="ds_local" value="<?php echo $ponto->ds_local; ?>" />

            <label for="nm_ponto">Nome do ponto:</label>
            <input name="nm_ponto" type="text" id="nm_ponto" value="<?php echo $ponto->nm_ponto; ?>" />
        
            <label for="cd_tipo_ponto">Tipo de ponto:</label>
            <select id="cd_tipo_ponto" name="cd_tipo_ponto">
                <?php
                    if (isset($tipo_ponto) && is_array($tipo_ponto))
                    {
                        foreach ($tipo_ponto as $row_tipo)
                        {
                ?>
                           <option value="<?php echo $row_tipo->cd_tipo_ponto; ?>" <?php echo $row_tipo->cd_tipo_ponto == $ponto->cd_tipo_ponto ? 'selected="selected"' : NULL; ?>><?php echo utf8_decode($row_tipo->nm_tipo_ponto); ?></option>
                <?php
                        }
                    }
                ?>
            </select>
            
            
            <label for="in_status">Situa��o:</label>
            <select id="in_status" name="in_status">
                <option value="A" <?php echo $ponto->in_status == 'A' ? 'selected="selected"' : NULL; ?>>Ativo</option>                      
                <option value="I" <?php echo $ponto->in_status == 'I' ? 'selected="selected"' : NULL; ?>>Inativo</option>
            </select>
            
            <div class="form_button">
                <input name="salvar" type="submit" id="salvar" value="Salvar"  />
            </div>
        </fieldset> 
    </form>
</div>