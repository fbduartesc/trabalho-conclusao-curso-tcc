<?php if (isset($at_equipments) && is_array($at_equipments)) { ?>
	<table>
	    <thead>
	        <th>Equipamento</th>
	        <th>Status</th>
	        <th style="text-align: center;">N� de portas ativas</th>
	        <th style="text-align: center;">N� de portas defeituosas</th>
	        <th style="text-align: center;">N� de portas inativas</th>
	    </thead>
	    <tbody>
	    <?php            
            foreach ($at_equipments as $i => $row_at_equipment)
            {
                if ($i == 0 OR $row_at_equipment->cd_at != $at_equipments[$i - 1]->cd_at)
                {
                    echo "<tr><td colspan=\"5\" class=\"corsin\">{$row_at_equipment->nm_at} ({$row_at_equipment->nm_bloco})</td></tr>";
                }
                
                if ($i == 0 OR $row_at_equipment->cd_at_equipamento != $at_equipments[$i - 1]->cd_at_equipamento)
                {
                    $total_ativo    = 0;
                    $total_inativo  = 0;
                    $total_defeito  = 0;
                }
                
                if ($row_at_equipment->in_status_porta == 'A')
                {
                	$total_ativo = $row_at_equipment->total_portas;
                }
                elseif ($row_at_equipment->in_status_porta == 'D')
                {
                    $total_defeito = $row_at_equipment->total_portas;
                }
                elseif ($row_at_equipment->in_status_porta == 'I')
                {
                    $total_inativo = $row_at_equipment->total_portas;
                }
                
                if ( ! isset($at_equipments[$i + 1]) OR $row_at_equipment->cd_at_equipamento != $at_equipments[$i + 1]->cd_at_equipamento)
                {
        ?>
	        <tr>
	            <td><?php echo "{$row_at_equipment->nm_equipamento} ($row_at_equipment->ds_modelo)"; ?></td>
	            <td><?php echo $row_at_equipment->in_status == 'A' ? 'Ativo' : 'Inativo'; ?></td>
	            <td align="center"><?php echo (int) $total_ativo; ?></td>
	            <td align="center"><?php echo (int) $total_defeito; ?></td>
	            <td align="center"><?php echo (int) $total_inativo; ?></td>
	        </tr>
        <?php
                }
            }
        ?>
    </tbody>
</table>
<?php } ?>