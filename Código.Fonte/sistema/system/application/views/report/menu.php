<div id="sub_menu">
    <ul>
        <li <?php echo $this->uri->segment(2) == 'at' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/at" title="ATs">ATs</a></li>
        <li <?php echo $this->uri->segment(2) == 'at_equipment' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/at_equipment" title="ATs x Equipamentos">ATs x Equipamentos</a></li>
        <li <?php echo $this->uri->segment(2) == 'point' && $this->uri->segment(3) == 'A' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/point/A" title="Pontos Ativos">Pontos Ativos</a></li>
        <li <?php echo $this->uri->segment(2) == 'point' && $this->uri->segment(3) == 'I' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/point/I" title="Pontos Inativos">Pontos Inativos</a></li>
        <li <?php echo $this->uri->segment(2) == 'door' && $this->uri->segment(3) == 'A' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/door/A" title="Portas Ativas">Portas Ativas</a></li>
        <li <?php echo $this->uri->segment(2) == 'door' && $this->uri->segment(3) == 'D' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/door/D" title="Portas Defeituosas">Portas Defeituosas</a></li>
        <li <?php echo $this->uri->segment(2) == 'door' && $this->uri->segment(3) == 'I' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/door/I" title="Portas Inativas">Portas Inativas</a></li>
    </ul>
</div>