<?php if (isset($portas) && is_array($portas)) { ?>
    <option value="0">Selecione a porta do equipamento</option>
    <?php
        foreach ($portas as $porta)
        {
             if ($porta->in_status == 'I')
             {
                $situacao = 'Inativo';
             }
             else
             {
                $situacao = 'Ativo';
             }
             
             if ($porta->is_certificado == 'N')
             {
                $certificado = 'N�o';
             }
             else
             {
                $certificado = 'Sim';
             }
    ?>
        <option value="<?php echo $porta->cd_at_equipamento_porta; ?>"><?php echo "{$porta->nr_porta} - {$situacao} - {$certificado}"; ?></option>                       
<?php
        }
    } else {
?>
    <option value="0">N�o h� portas para este equipamento</option>
<?php } ?>