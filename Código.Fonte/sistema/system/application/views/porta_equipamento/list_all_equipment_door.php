<?php
    $last_position = 1;
    
    foreach ($portas as $i => $porta)
    {
        if ($porta->in_status == 'A')
        {
            $situacao = 'verde';
        }
        elseif ($porta->in_status == 'I')
        {
            $situacao = 'preto';
        }
        elseif ($porta->in_status == 'D')
        {
            $situacao = 'vermelho';
        }
        
        if ($i == 0 OR $porta->cd_at_equipamento != $portas[$i - 1]->cd_at_equipamento)
        {
            if ($porta->nr_posicao > $last_position)
            {
                while($last_position < $porta->nr_posicao)
                {
                      echo '<div id="corpo"><div id="vazio"></div></div>';
                      $last_position++;
                }
            }
            else
            {
                $last_position++;
            }

            echo '<div id="corpo"><ul>';
        }
?>
        <li><a class="<?php echo $situacao; ?>" id="porta_<?php echo $porta->cd_at_equipamento_porta; ?>" href="javascript:void(0);" onclick="update_status_door(<?php echo $porta->cd_at_equipamento_porta; ?>);" title="Porta <?php echo $porta->nr_porta; ?>">&nbsp;</a></li>
<?php
        if ( ! isset($portas[$i + 1]) OR $porta->cd_at_equipamento != $portas[$i + 1]->cd_at_equipamento )
        {
            echo '</ul><div>';
            echo "<p>{$porta->nm_equipamento} ({$porta->ds_modelo}) - ";
            echo '<a href="javascript:void(0);" onclick="show_form_at_equipment(' . $porta->cd_at_equipamento . ');">Editar</a>';
            echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
            echo '<a href="javascript:void(0);" onclick="remove_at_equipment(' . $porta->cd_at_equipamento . ');">Excluir</a></p>';
            echo '</div></div>';
        }
    }
    
    if ($last_position < $porta->nr_tamanho)
    {
        while($last_position <= $porta->nr_tamanho)
        {
            echo '<div id="corpo"><div id="vazio"></div></div>';
            $last_position++;
        }
    }
?>