<div id="formulario">
    <div id="msg_envio_form"></div>
	<form action="<?php echo base_url(); ?>index.php/at/add" id="CadAt" name="CadAt" method="post" onsubmit="return save_form(this, '#dialog-at');" >
		<fieldset>
			<legend>Armário de telecomunicação - Adicionar</legend>
			
			<input type="hidden" id="ds_posicao" name="ds_posicao" value="" />
			<input type="hidden" id="nr_identificacao" name="nr_identificacao" value="" />
			
			<label for="nm_at">Nome AT:</label>
			<input name="nm_at" type="text" id="nm_at" value="<?php if(empty($msg_cadastro)) { echo $this->validation->nm_at; }?>" />
			
			<label for="ds_local">Local de armazenamento do AT:</label>
			<input name="ds_local" type="text" id="ds_local" value="<?php if(empty($msg_cadastro)) { echo $this->validation->ds_local; }?>" />
			
			<label for="nr_tamanho">Tamanho (U):</label>
			<input name="nr_tamanho" type="text" id="nr_tamanho" value="<?php if(empty($msg_cadastro)) { echo $this->validation->nr_tamanho; }?>" />
			
			<label for="cd_bloco">Bloco:</label>
			<select id="cd_bloco" name="cd_bloco">
                <option value="">Selecione um bloco</option>
                <?php
                    if (isset($blocos) && is_array($blocos))
                    {
                        foreach ($blocos as $row_bloco)
                        {
                            echo '<option value="' . $row_bloco->cd_bloco . '">' . utf8_decode($row_bloco->nm_bloco) . '</option>';                      
                        }
                    }
                ?>
			</select>
			
			<div class="form_button">
                <input name="adicionar" type="submit" id="adicionar" value="Adicionar"  />
                <input name="limpar" type="reset" id="limpar" value="Limpar" />
            </div>
		</fieldset>	
	</form>		
</div>