<div id="formulario">
    <div id="msg_envio_form"></div>
    <form action="<?php echo base_url(); ?>index.php/at/edit" id="CadAt" name="CadAt" method="post" onsubmit="return save_form(this, '#dialog-at');" >
        <fieldset>
            <legend>Armário de telecomunicação - Editar</legend>
            
            <input type="hidden" id="cd_at" name="cd_at" value="<?php echo $at->cd_at; ?>" />
            <input type="hidden" id="ds_posicao" name="ds_posicao" value="<?php echo $at->ds_posicao; ?>" />
            <input type="hidden" id="nr_identificacao" name="nr_identificacao" value="<?php echo $at->nr_identificacao; ?>" />
            
            <label for="nm_at">Nome AT:</label>
            <input name="nm_at" type="text" id="nm_at" value="<?php echo $at->nm_at; ?>" />
            
            <label for="ds_local">Local de armazenamento do AT:</label>
            <input name="ds_local" type="text" id="ds_local" value="<?php echo $at->ds_local; ?>" />
            
            <label for="nr_tamanho">Tamanho (U):</label>
            <input name="nr_tamanho" type="text" id="nr_tamanho" value="<?php echo $at->nr_tamanho; ?>" />
            
            <label for="cd_bloco">Bloco:</label>
            <select id="cd_bloco" name="cd_bloco">
                <option value="">Selecione um bloco</option>
                <?php
                    if (isset($blocos) && is_array($blocos))
                    {
                        foreach ($blocos as $row_bloco)
                        {
                ?>
                            <option value="<?php echo $row_bloco->cd_bloco; ?>"  <?php echo $row_bloco->cd_bloco == $at->cd_bloco ? 'selected="selected"' : NULL; ?>><?php echo utf8_decode($row_bloco->nm_bloco); ?></option>
                <?php
                        }
                    }
                ?>
            </select>
            
            <div class="form_button">
                <input name="salvar" type="submit" id="salvar" value="Salvar"  />
            </div>
        </fieldset> 
    </form>
    <div id="list_equipment">
	    <form action="" onsubmit="return false;">
		    <fieldset>
		        <legend>Equipamentos</legend>
		        <p>Aguarde, carregando equipamentos...</p>
		    </fieldset>
	    </form>
    </div>
</div>