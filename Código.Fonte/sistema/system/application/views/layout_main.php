<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Vertebrata  
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20100423

-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Sispoint - Administra��o dos Pontos de Rede</title>
	
	<link href="<?php echo base_url();?>public/css/jqueryui/ui.all.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo base_url();?>public/css/style.css" rel="stylesheet" type="text/css" media="screen" />
	<script language="JavaScript" type="text/javascript">
	 // <! [CDATA[
	        var host = "<?php echo site_url();?>";      
	        var base = "<?php echo base_url();?>";
	 // ]] >
	</script>
	<script src="<?php echo base_url();?>public/js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>public/js/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>public/js/ui.multiselect.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>public/js/jquery.scrollTo-min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>/public/lib/OpenLayers.js"></script>
	
    <script type="text/javascript"> 
		// Utilizado para criar a funcao de clique
		// =========================
        OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {          
            defaultHandlerOptions: {
                'single': true,
                'double': false,
                'pixelTolerance': 0,
                'stopSingle': false,
                'stopDouble': false
                },
	 
            initialize: function(options) {
                this.handlerOptions = OpenLayers.Util.extend(
                    {}, this.defaultHandlerOptions
                );
                OpenLayers.Control.prototype.initialize.apply(
                    this, arguments
                ); 
                this.handler = new OpenLayers.Handler.Click(
                    this, {
                        'click': this.onClick,
                        'dblclick': this.onDblclick 
                    }, this.handlerOptions
                );
            }, 
	 
            onClick: function(evt) {
                var output = document.getElementById(this.key + "Output");
                var msg = "click " + evt.xy;

				alert(output.value + msg);
            },

            onDblclick: function(evt) {  
                var output = document.getElementById(this.key + "Output");
                var msg = "dblclick " + evt.xy;
                
                output.value = output.value + msg + "\n";
            }
        });
        // =========================
	
		var zoomify_width = 10000;
		var zoomify_height = 3042;
		var zoomify_url = "<?php echo base_url();?>public/images/adm_zdata/";
	    var map, zoomify, drawControls, pontoControl, controls, at, vlayer, zb;
	 
	    function init() {
	    	/* First we initialize the zoomify pyramid (to get number of tiers) */
	    	zoomify = new OpenLayers.Layer.Zoomify( "Bloco Administrativo", zoomify_url, new OpenLayers.Size(zoomify_width, zoomify_height));
					
            /* Map with raster coordinates (pixels) from Zoomify image */
            var options = {
                controls: [
                    new OpenLayers.Control.Navigation(),
			        new OpenLayers.Control.KeyboardDefaults(),
			        new OpenLayers.Control.PanZoomBar(),
			        new OpenLayers.Control.Scale(),
			        new OpenLayers.Control.Attribution()
                ],
	            maxExtent: new OpenLayers.Bounds(0, 0, zoomify_width, zoomify_height),
	            maxResolution: Math.pow(2, zoomify.numberOfTiers-1),
	            numZoomLevels: zoomify.numberOfTiers,
	            units: 'pixels'
            };
				
            map = new OpenLayers.Map("map", options);
	       
            var click = new OpenLayers.Control.Click({
								handlerOptions: {
		                            "single": true							
		                        }
            });
            
            map.addControl(click);
            
            var saveStrategy = new OpenLayers.Strategy.Save();
            saveStrategy.events.register('success', null, saveSuccess);
            saveStrategy.events.register('fail', null, saveFail);
            
            vlayer = new OpenLayers.Layer.Vector('Ponto');
            
            var pontoLayer = new OpenLayers.Layer.Vector('Pontos de rede');
            map.addLayer(pontoLayer);
            
            var atLayer = new OpenLayers.Layer.Vector('Arm�rios de telecomunica��es');
            map.addLayer(atLayer);
		   
			// Aqui deve ser feito a busca na base de dados e

			/* Carrega os objetos do tipo at e cria-os e inseri-os no mapa */
			$.ajax({
                type: 'POST',
                url: "<?php echo base_url();?>index.php/at/list_all/",
                beforeSend: function() {
				    // loading...
			    },
                success: function(data) {
				    var posicoes = data.split("|");
				    var i = 0;
				    
					for(i=0; i<(posicoes.length); i++) {				
						atLayer.addFeatures([new OpenLayers.Feature.Vector(
							OpenLayers.Geometry.fromWKT(
								posicoes[i]
								)
							)
						]);
					}				
                },
				error: function (data) {
					alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
				}
            });
		
			/* Carrega os pontos de rede */
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url();?>index.php/ponto/list_all/",		
				beforeSend: function() {
                    // loading...
				},
				success: function(data) {				
					var posicoes = data.split("|");			
					var i = 0;
							
					for(i=0; i<(posicoes.length); i++) {		
						pontoLayer.addFeatures([new OpenLayers.Feature.Vector(
							OpenLayers.Geometry.fromWKT(
								posicoes[i]
								)
							)
						]);			
					}
				},
				error: function (data) {
					alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
				}
			});
			
			selectControl = new OpenLayers.Control.SelectFeature(
	                [pontoLayer, atLayer],
	                {
	                    clickout: true, toggle: false,
	                    multiple: false, hover: false,
	                }
            );
	            
	        map.addControl(selectControl);
	        selectControl.activate();
				
			polyOptions = {sides: 3};
			atOptions = {sides: 4};
			
			pontoControl = new OpenLayers.Control.DrawFeature(pontoLayer,
		           			  OpenLayers.Handler.RegularPolygon,
				              {title: "Adicionar ponto de rede", handlerOptions: {sides: '3', angle: '0'}});
				              
            atControl = new OpenLayers.Control.DrawFeature(atLayer,
					       OpenLayers.Handler.RegularPolygon,
						   {title: "Adicionar Arm�rio de telecomunica��o", handlerOptions: {sides: '4', angle: '0'}});

            var apaga = atLayer;
				
			zb = new OpenLayers.Control.ZoomBox({title:'Zoom box: Selecting it you can zoom on an area by clicking and dragging.'});
	
            var panel = new OpenLayers.Control.Panel({defaultControl: zb});
            
			panel.addControls([
                new OpenLayers.Control.MouseDefaults({title:'You can use the default mouse configuration'}), 
				zb,
				new OpenLayers.Control.ZoomToMaxExtent({title:"Zoom to the max extent"}),
				pontoControl,
				atControl,
				new DeleteFeature(pontoLayer, {title: 'Remover Ponto'}),
				new DeleteFeature(atLayer, {title: 'Remover AT'})
			]);

            map.addControl(panel);
			map.addLayer(zoomify);
	        map.addControl(new OpenLayers.Control.LayerSwitcher());
	        
			// Habilita mexer o mapa com a utiliza��o do mouse e scroll
			map.addControl(new OpenLayers.Control.MouseDefaults()); 
			
			map.setCenter(new OpenLayers.LonLat(5, 40), 5); 
			
			/*pontoLayer.events.on({
                "featureselected": function(e) {
	               showStatus("selected feature "+e.feature.id+" on Vector Layer 1");
                },
	            "featureunselected": function(e) {
	               showStatus("unselected feature "+e.feature.id+" on Vector Layer 1");
                }
            });*/
            
	        atLayer.events.on({
                "featureselected": function(e) {
				    show_form_edit('#dialog-at', e.feature.geometry);
                },
	            "featureunselected": function(e) {
	                showStatus("");
                }
            });
				
			pontoLayer.events.on({
                "featureselected": function(e) {						
                    show_form_edit('#dialog-ponto', e.feature.geometry);
                },
                "featureunselected": function(e) {
                    showStatus("");
                }
            });
	
			map.zoomToMaxExtent();
					
			/*
			*****************************************
			Configurar tamanho do ponto de rede e AT.
			*/
			
			var radius = parseFloat(0.001) * map.getExtent().getHeight();
			pontoControl.handler.setOptions({radius: radius, title:"Ponto"});
			var radius1 = parseFloat(0.001) * map.getExtent().getHeight();
			atControl.handler.setOptions({radius: radius1, title:"At"});
	
			function saveSuccess(event) {
	            alert('Changes saved');
	        }
	        
	        function saveFail(event) {
	            alert('Error! Changes not saved');
	        }
	        
	        function dataLoaded(event) {
	            this.map.zoomToExtent(event.object.getDataExtent());
	        }
	        
	        function formatLonlats(lonLat) {
                var lat = lonLat.lat;
			    var long = lonLat.lon;
			    var ns = OpenLayers.Util.getFormattedLonLat(lat);
			    var ew = OpenLayers.Util.getFormattedLonLat(long,'lon');
			    
			    return ns + ', ' + ew + ' (' + (Math.round(lat * 10000) / 10000) + ', ' + (Math.round(long * 10000) / 10000) + ')';
            }
        }

    	function verifica() {
    	   alert('seleciona'); 
        }
        
	    DeleteFeature = OpenLayers.Class(OpenLayers.Control, {
		    initialize: function(layer, options) {
		        OpenLayers.Control.prototype.initialize.apply(this, [options]);
		        this.layer = layer;		        
		        this.handler = new OpenLayers.Handler.Feature(
		            this, layer, {click: this.clickFeature}
		        );
		    },
            clickFeature: function(feature) {
                if(confirm('Deseja excluir o objeto?')) {
					/* if feature doesn't have a fid, destroy it
					if(feature.fid == undefined) {
					
					if(this.layer.name == "Arm�rios de telecomunica��es")
					{			
						alert("apagando AT: "+this.feature.id);
						$("#identificacao").val(+feature.id); */
                    if(feature.fid == undefined)
					{			
						//alert(feature.geometry);
						if(this.layer.name == 'Pontos de rede') {
							remove_object('#dialog-ponto', feature.geometry);
						} else if(this.layer.name == 'Arm�rios de telecomunica��es') {
							remove_object('#dialog-at', feature.geometry);
						}
						
						this.layer.destroyFeatures([feature]);
					} else {
						feature.state = OpenLayers.State.DELETE;
						this.layer.events.triggerEvent("afterfeaturemodified", {feature: feature});
						feature.renderIntent = "select";
						
						//apagar dados do objeto no banco
						//alert("feature");	
						
						this.layer.drawFeature(feature);		
					}
                }
            },
		    setMap: function(map) {
		        this.handler.setMap(map);
		        OpenLayers.Control.prototype.setMap.apply(this, arguments);
		    },
		    CLASS_NAME: "OpenLayers.Control.DeleteFeature"
		})
	
        function createFeatures() {
            var extent = map.getExtent();
            var features = [];
            for(var i=0; i<10; ++i) {
                features.push(new OpenLayers.Feature.Vector(
                    new OpenLayers.Geometry.Point(extent.left + (extent.right - extent.left) * Math.random(),
                    extent.bottom + (extent.top - extent.bottom) * Math.random()
                )));
            }
            return features;
        }
        
        function showStatus(text) {			
            document.getElementById("status").innerHTML = text;            
        }
	
		$(function() {
		    $("#abas").tabs();
		    $("#abas_info").tabs();
		    $("#abas_info_ponto").tabs();
		});
    </script>
    
	<script type="text/javascript" src="<?php echo base_url();?>public/js/util.js"></script>
</head>

<body onload="init();">
	<div id="wrapper">
		
		<div id="header">
			<div id="logo">
				<h1><a href="#">SisPoint   </a></h1>
				<p> administra��o dos pontos de rede</a></p>
			</div>
		</div>
		<!-- end #header -->
		
		<div id="menu">
			<ul>
				<li <?php echo $this->uri->segment(1) != 'report' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>">In�cio</a></li>
                <li <?php echo $this->uri->segment(1) == 'report' ? 'class="current_page_item"' : NULL; ?>><a href="<?php echo base_url(); ?>index.php/report/">Relat�rios</a></li>
			</ul>
		</div>
		<!-- end #menu -->
		<div id="page"><?php echo $content_for_layout;?></div>
		<!-- end #page -->
	</div>
</body>
</html>