<form action="<?php echo base_url(); ?>index.php/at_equipamento/add" onsubmit="return save_equipment(this);">
    <fieldset>
        <legend>Equipamentos - adicionar</legend>
        
        <div class="form_equipment" style="float:left;margin-right:80px;">
            <label for="cd_at">AT:</label>
            <select id="cd_at" name="cd_at">
                <option value="">Selecione um at</option>
                <?php
                    if (isset($ats) && is_array($ats))
                    {
                        foreach ($ats as $row_at)
                        {
                ?>
                            <option value="<?php echo $row_at->cd_at; ?>" <?php echo $cd_at == $row_at->cd_at ? 'selected="selected"' : NULL; ?>><?php echo utf8_decode($row_at->nm_at); ?></option>
                <?php
                        }
                    }
                ?>
            </select>
        
            <label for="cd_equipamento">Equipamento:</label>
            <select id="cd_equipamento" name="cd_equipamento">
                <option value="">Selecione um equipamento</option>
                <?php
                    if (isset($equipamentos) && is_array($equipamentos))
                    {
                        foreach ($equipamentos as $row_equipamento)
                        {
                ?>
                            <option value="<?php echo $row_equipamento->cd_equipamento; ?>"><?php echo utf8_decode($row_equipamento->nm_equipamento) . ' - ' . utf8_decode($row_equipamento->ds_modelo); ?></option>
                <?php
                        }
                    }
                ?>
            </select>
            
            <label for="nr_posicao">Posi��o:</label>
            <input name="nr_posicao" type="text" id="nr_posicao" value="<?php if(empty($msg_cadastro)) { echo $this->validation->nr_posicao; }?>" />
            
            <div class="form_button">
                <input name="adicionar" type="submit" id="adicionar" value="Adicionar"  />
                <input name="limpar" type="reset" id="limpar" value="Limpar" />
            </div>
        </div>
        <div class="form_equipment">
            <label for="ds_serie">S�rie:</label>
            <input name="ds_serie" type="text" id="ds_serie" value="<?php if(empty($msg_cadastro)) { echo $this->validation->ds_serie; }?>" />
            
            <label for="in_status">Situa��o:</label>
            <select id="in_status" name="in_status">
                <option value="A">Ativo</option>                      
                <option value="I">Inativo</option>
            </select>
        </div>
    </fieldset>
</form>
<form action="" onsubmit="return false;">
    <fieldset>
        <legend>Equipamentos - editar</legend>
        <div id="equipments"><?php echo $at_equipamentos; ?></div>
    </fieldset>
</form>
<?/* 
<form action="<?php echo base_url(); ?>index.php/at_equipamento/add" id="CadAtEquipamentos" name="CadAtEquipamentos" method="post" onsubmit="return salva_at_equipamento(this);" >
<fieldset>
	<legend>Equipamentos por AT - Adicionar</legend>
	
		Equipamento<br />
		<select id="equipamento" name="equipamento">
			<?php
	
			foreach($equipamento as $row_equipamento):
				echo "<option value='".$row_equipamento->id."'>".utf8_decode($row_equipamento->equipamento)." - "
				.$row_equipamento->nm_portas." portas (".utf8_decode($row_equipamento->modelo).")</option>";
			endforeach;
			?>	
		</select><br />		
		
	    <label for="posicao">Posi��o do equipamento</label>
		<input name="posicao" type="text" id="posicao" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->posicao; }?>" /><br />
		<?php echo "<div class='erro'><strong>".$this->validation->posicao_error."</strong></div>"; ?>
		<br />
		<label for="nm_serie">N�mero de s�rie</label>
		<input name="nm_serie" type="text" id="nm_serie" value="" /><br />
		
		<label for="modelo">Modelo</label>
		<input name="modelo" type="text" id="modelo" value="" /><br />
		
		Situa��o<br />
		<input name="situacao" type="radio" id="situacao" value="A" CHECKED = "CHECKED" />Ativo
		<input name="situacao" type="radio" id="situacao" value="I" />Inativo
		
		<input type='hidden' id='at_id' name="at_id" value="" />
		<input name="adicionar" type="submit" id="adicionar" value="Adicionar" />
		<input name="limpar" type="reset" id="limpar" value="Limpar" />

</fieldset>	
</form>
*/ ?>
