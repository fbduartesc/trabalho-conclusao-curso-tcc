<form action="<?php echo base_url(); ?>index.php/at_equipamento/edit" onsubmit="return save_equipment(this);">
    <fieldset>
        <legend>Equipamentos - adicionar</legend>
        
        <input type="hidden" name="cd_at_equipamento" value="<?php echo $at_equipamento->cd_at_equipamento; ?>" />
        
        <div class="form_equipment" style="float:left;margin-right:80px;">
            <label for="cd_at">AT:</label>
            <select id="cd_at" name="cd_at">
                <option value="">Selecione um at</option>
                <?php
                    if (isset($ats) && is_array($ats))
                    {
                        foreach ($ats as $row_at)
                        {
                ?>
                            <option value="<?php echo $row_at->cd_at; ?>" <?php echo $at_equipamento->cd_at == $row_at->cd_at ? 'selected="selected"' : NULL; ?>><?php echo utf8_decode($row_at->nm_at); ?></option>
                <?php
                        }
                    }
                ?>
            </select>
        
            <label for="cd_equipamento">Equipamento:</label>
            <select id="cd_equipamento" name="cd_equipamento">
                <option value="">Selecione um equipamento</option>
                <?php
                    if (isset($equipamentos) && is_array($equipamentos))
                    {
                        foreach ($equipamentos as $row_equipamento)
                        {
                ?>
                            <option value="<?php echo $row_equipamento->cd_equipamento; ?>" <?php echo $at_equipamento->cd_equipamento == $row_equipamento->cd_equipamento ? 'selected="selected"' : NULL; ?>><?php echo utf8_decode($row_equipamento->nm_equipamento) . ' - ' . utf8_decode($row_equipamento->ds_modelo); ?></option>
                <?php
                        }
                    }
                ?>
            </select>
            
            <label for="nr_posicao">Posi��o:</label>
            <input name="nr_posicao" type="text" id="nr_posicao" value="<?php echo $at_equipamento->nr_posicao; ?>" />
            
            <div class="form_button">
                <input name="adicionar" type="submit" id="adicionar" value="Adicionar"  />
                <input name="limpar" type="reset" id="limpar" value="Limpar" />
            </div>
        </div>
        <div class="form_equipment">
            <label for="ds_serie">S�rie:</label>
            <input name="ds_serie" type="text" id="ds_serie" value="<?php echo $at_equipamento->ds_serie; ?>" />
            
            <label for="in_status">Situa��o:</label>
            <select id="in_status" name="in_status">
                <option value="A" <?php echo $at_equipamento->in_status == 'A' ? 'selected="selected"' : NULL; ?>>Ativo</option>                      
                <option value="I" <?php echo $at_equipamento->in_status == 'I' ? 'selected="selected"' : NULL; ?>>Inativo</option>
            </select>
        </div>
    </fieldset>
</form>
<form action="" onsubmit="return false;">
    <fieldset>
        <legend>Equipamentos - editar</legend>
        <div id="equipments"><?php echo $at_equipamentos; ?></div>
    </fieldset>
</form>