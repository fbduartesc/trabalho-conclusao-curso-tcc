<?php

$lang['required']           = "O campo \"%s\" � obrigat�rio.";
$lang['isset']              = "O campo \"%s\" deve conter um valor.";
$lang['valid_email']        = "O campo \"%s\" deve conter um endere�o de email v�lido.";
$lang['valid_emails']       = "O campo \"%s\" deve conter endere�os de email v�lidos.";
$lang['valid_url']          = "O campo \"%s\" deve conter uma URL v�lida.";
$lang['valid_ip'] 		    = "O campo \"%s\" deve conter um IP v�lido.";
$lang['min_length']         = "O campo \"%s\" deve ter pelo menos \"%s\" caracteres de comprimento.";
$lang['max_length']         = "O campo \"%s\" n�o pode exceder \"%s\" caracteres de comprimento.";
$lang['exact_length']       = "O campo \"%s\" deve conter exatamente \"%s\" caracteres de comprimento.";
$lang['alpha']              = "O campo \"%s\" deve conter apenas caracteres alfa-num�ricos.";
$lang['alpha_numeric']      = "O campo \"%s\" deve conter apenas caracteres alfa-num�ricos ou n�meros";
$lang['alpha_dash']         = "O campo \"%s\" deve conter apenas caracteres alfa-num�ricos, n�meros, underscores, e h�fen.";
$lang['numeric']            = "O campo \"%s\" deve conter apenas n�mero(s).";
$lang['is_numeric']		    = "O campo \"%s\" deve conter apenas n�mero(s).";
$lang['integer']		    = "O campo \"%s\" deve conter apenas n�mero(s) inteiro(s).";
$lang['matches']            = "O campo \"%s\" deve ser igual ao campo \"%s\".";
$lang['is_natural']		    = "O campo \"%s\" deve conter apenas n�mero(s).";
$lang['is_natural_no_zero']	= "O campo \"%s\" deve conter apenas n�mero(s) maiores que zero.";

/* End of file validation_lang.php */
/* Location: ./system/language/pt_br/validation_lang.php */