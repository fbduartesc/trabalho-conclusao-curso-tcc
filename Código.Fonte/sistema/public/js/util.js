var operation_status = false;

function show_form_add(p, f, tipo, ds_posicao, nr_identificacao) {
	var url_mostra_formulario = base;
	operation_status = false;
	
    if (tipo == '#dialog-at') {
		url_mostra_formulario = url_mostra_formulario + 'index.php/at/get_form_add/';
    } else {
		url_mostra_formulario = url_mostra_formulario + 'index.php/ponto/get_form_add/';
	}
	
	$.ajax({
        type: 'POST',
        url: url_mostra_formulario,
        data: '',
		dataType: 'json',
        beforeSend: function() {
            // loading...
        },
        success: function(data) {
			if (data.error == null) {
				$('#window_lightbox').html(data.conteudo).dialog({
					title: 'Sispoint - Administra��o dos pontos de rede',
					draggable: false,
					modal: true,
					resizable: false,
					width: 800,
					height: 600,
					overlay: {
						opacity: 0.5,
						background: '#FFF'
					},
					close: function(){
					
						if (operation_status == false) {
							p.removeFeatures([f]);
							//alert('excluindo camada');
						}
						
						$(this).dialog('destroy');
						$('window_lightbox').hide();
					}
				}).show();
				
				$('#ds_posicao').val(ds_posicao);
				$('#nr_identificacao').val(nr_identificacao);
				
				$('div.ui-dialog').css('z-index', '1020');
			} else {
				alert(data.error);
			}
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
        }
    });
}

function show_form_edit(tipo, ds_posicao, message) {
    var url_mostra_formulario = base;
    operation_status = false;
    
    if (tipo == '#dialog-at') {
        url_mostra_formulario = url_mostra_formulario + 'index.php/at/get_form_edit/';
    } else {
        url_mostra_formulario = url_mostra_formulario + 'index.php/ponto/get_form_edit/';
    }
    
    $.ajax({
        type: 'POST',
        url: url_mostra_formulario,
        data: 'ds_posicao=' + ds_posicao,
        dataType: 'json',
        beforeSend: function() {
            // loading...
        },
        success: function(data) {
            if (data.error == null) {
				if ($('#window_lightbox').is(':hidden')) {
					$('#window_lightbox').html(data.conteudo).dialog({
						title: 'Sispoint - Administra��o dos pontos de rede',
						draggable: false,
						modal: true,
						resizable: false,
						width: 800,
						height: 600,
						overlay: {
							opacity: 0.5,
							background: '#FFF'
						},
						close: function(){
							$(this).dialog('destroy');
							$('window_lightbox').hide();
						}
					}).show();
					
					$('div.ui-dialog').css('z-index', '1020');
				} else {
					$('#window_lightbox').html(data.conteudo);
				}
				
				if(message != null) {
                    $('#msg_envio_form').html(message);
                }
				
				if (tipo == '#dialog-at') {
					show_form_at_equipment();
				}
            } else {
                alert(data.error);
            }
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
        }
    });
}

function show_form_at_equipment(cd_at_equipamento) {
	var url_mostra_formulario = base;
	cd_at_equipamento = parseInt(cd_at_equipamento);
    
    if (isNaN(cd_at_equipamento)) {
        url_mostra_formulario = url_mostra_formulario + 'index.php/at_equipamento/get_form_add/';
    } else {
        url_mostra_formulario = url_mostra_formulario + 'index.php/at_equipamento/get_form_edit/';
    }
    
    $.ajax({
        type: 'POST',
        url: url_mostra_formulario,
        data: 'cd_at=' + $('input[name=cd_at]').val() + '&cd_at_equipamento=' + cd_at_equipamento,
        dataType: 'json',
        beforeSend: function() {
            $('div#list_equipment').empty().html('Aguarde. Carregando equipamentos...');
        },
        success: function(data) {
            if (data.error == null) {
                $('div#list_equipment').empty().html(data.conteudo);
            } else {
                alert(data.error);
            }
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
        }
    });
}

function save_form(obj, tipo) {
    $.ajax({
        type: 'POST',
        url: obj.action,
        data: $(obj).serialize(),
        dataType: 'json',
        beforeSend: function() {
            // loading...
        },
        success: function(data) {
            if (data.error != null)
            {
                $('#msg_envio_form').html(data.error);
            } else {
				$('#msg_envio_form').html(data.loading);
				$('#window_lightbox form').empty();
				
				show_form_edit(tipo, data.ds_posicao, data.success);
				
				operation_status = true;
            }
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
        }
    });

    return false;
}

function save_equipment(obj) {
    $.ajax({
        type: 'POST',
        url: obj.action,
        data: $(obj).serialize(),
        dataType: 'json',
        beforeSend: function() {
            $('div#equipments').empty().html('Aguarde. Carregando equipamentos...');
        },
        success: function(data) {
            if (data.error != null)
            {
                $('#msg_envio_form').html(data.error);
            } else {
                $('#msg_envio_form').html(data.success);
                $('div#equipments').empty().html(data.conteudo);
				
				clear_form_equipment();
            }
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
        }
    });

    return false;
}

function clear_form_equipment(){
	$('input[name=cd_at_equipamento]').val('');
	$('select[name=cd_equipamento] option').removeAttr('disabled').removeAttr('selected');
	$('input[name=nr_posicao]').val('');
	$('input[name=ds_serie]').val('');
	$('input[name=ds_modelo]').val('');
	$('select[name=in_status] option').removeAttr('selected');	
}

function remove_object(tipo, ds_posicao) {
	var url_mostra_formulario = base;
    
    if (tipo == '#dialog-at') {
        url_mostra_formulario = url_mostra_formulario + 'index.php/at/remove/';
    } else {
        url_mostra_formulario = url_mostra_formulario + 'index.php/ponto/remove/';
    }
	
    $.ajax({
        type: 'POST',
        url: url_mostra_formulario,
		data: 'ds_posicao=' + ds_posicao,
		dataType: 'json',
        beforeSend: function() {
            // loading...
        },
        success: function(data){
			if (data.error == null) {
				alert(data.success);
			} else {
				alert(data.error);
			}
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');           
        }
    });
}

function remove_at_equipment(cd_at_equipamento){
	if (confirm('Deseja realmente excluir este equipamento e todas as liga��es com os pontos?')) {
		$.ajax({
			type: 'POST',
			url: base + 'index.php/at_equipamento/remove/',
			data: 'cd_at_equipamento=' + cd_at_equipamento + '&cd_at=' + $('input[name=cd_at]').val(),
			dataType: 'json',
			beforeSend: function(){
				$('div#equipments').empty().html('Aguarde. Carregando equipamentos...');
			},
			success: function(data){
				if (data.error == null) {
					alert(data.success);
				}
				else {
					alert(data.error);
				}
				$('div#equipments').empty().html(data.conteudo);
			},
			error: function(data){
				alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
			}
		});
	}
}

function get_all_at_equipamento(obj) {
	$('select[name=select_equipamento]').html('<option value="0">Carregando...</option>');
	$('select[name=select_porta]').html('<option value="0">Selecione a porta do equipamento</option>');
	
	$.ajax({
        type: 'POST',
        url: base + 'index.php/at_equipamento/ajax_get_all_by_at_code/',
        data: 'cd_at=' + $(obj).val(),
        dataType: 'json',
        beforeSend: function() {
            // loading...
        },
        success: function(data) {
            if (data.error != null)
            {
                $('#msg_envio_form').html(data.error);
            }
			
			$('select[name=select_equipamento]').html(data.conteudo);
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
        }
    });

    return false;
}

function get_all_porta_equipamento(obj) {
    $('select[name=cd_at_equipamento_porta]').html('<option value="0">Carregando...</option>');
    
    $.ajax({
        type: 'POST',
        url: base + 'index.php/porta_equipamento/ajax_get_all_porta_equipamento/',
        data: 'cd_at_equipamento=' + $(obj).val(),
        dataType: 'json',
        beforeSend: function() {
            // loading...
        },
        success: function(data) {
            if (data.error != null)
            {
                $('#msg_envio_form').html(data.error);
            }
            
            $('select[name=cd_at_equipamento_porta]').html(data.conteudo);
        },
        error: function (data) {
            alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
        }
    });

    return false;
} 

function update_status_door(cd_at_equipamento_porta) {
	var in_status = prompt('Informe a situa��o desejada:\n' + 
	                       'A - Ativo\n' + 
						   'D - Desativado\n' + 
						   'I - Inativo').toUpperCase();
    
	if (in_status == 'A' || in_status == 'D' || in_status == 'I') {
		$.ajax({
			type: 'POST',
			url: base + 'index.php/porta_equipamento/ajax_update_status_door/',
			data: 'cd_at_equipamento_porta=' + cd_at_equipamento_porta + '&in_status=' + in_status,
			dataType: 'json',
			beforeSend: function(){
			// loading...
			},
			success: function(data){
				if (data.error != null) {
					$('#msg_envio_form').html(data.error);
				}
				else {
					$('#porta_' + cd_at_equipamento_porta).removeClass().addClass(data.color);
				}
			},
			error: function(data){
				alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
			}
		});
	} else {
		alert('Informe uma situa��o v�lida!');
	}

    return false;
}