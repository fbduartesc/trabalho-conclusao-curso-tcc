/* Copyright (c) 2006-2008 MetaCarta, Inc., published under the Clear BSD
 * license.  See http://svn.openlayers.org/trunk/openlayers/license.txt for the
 * full text of the license. */


/**
 * @requires OpenLayers/Control.js
 * @requires OpenLayers/Feature/Vector.js
 */

/**
 * Class: OpenLayers.Control.DrawFeature
 * The DrawFeature control draws point, line or polygon features on a vector
 * layer when active.
 *
 * Inherits from:
 *  - <OpenLayers.Control>
 */
 var posicao;
OpenLayers.Control.DrawFeature = OpenLayers.Class(OpenLayers.Control, {
    
    /**
     * Property: layer
     * {<OpenLayers.Layer.Vector>}
     */
    layer: null,

    /**
     * Property: callbacks
     * {Object} The functions that are sent to the handler for callback
     */
    callbacks: null,
    
    /**
     * Constant: EVENT_TYPES
     *
     * Supported event types:
     * featureadded - Triggered when a feature is added
     */
    EVENT_TYPES: ["featureadded"],
    
    /**
     * APIProperty: multi
     * {Boolean} Cast features to multi-part geometries before passing to the
     *     layer.  Default is false.
     */
    multi: false,

    /**
     * APIProperty: featureAdded
     * {Function} Called after each feature is added
     */
    featureAdded: function() {},

    /**
     * APIProperty: handlerOptions
     * {Object} Used to set non-default properties on the control's handler
     */
    handlerOptions: null,
    
    /**
     * Constructor: OpenLayers.Control.DrawFeature
     * 
     * Parameters:
     * layer - {<OpenLayers.Layer.Vector>} 
     * handler - {<OpenLayers.Handler>} 
     * options - {Object} 
     */
	 
	 
    initialize: function(layer, handler, options) {
	        
        // concatenate events specific to vector with those from the base
        this.EVENT_TYPES =
            OpenLayers.Control.DrawFeature.prototype.EVENT_TYPES.concat(
            OpenLayers.Control.prototype.EVENT_TYPES
        );
		
        
        OpenLayers.Control.prototype.initialize.apply(this, [options]);
        this.callbacks = OpenLayers.Util.extend(
            {
                done: this.drawFeature,
                modify: function(vertex, feature) {
                    this.layer.events.triggerEvent(
                        "sketchmodified", {vertex: vertex, feature: feature}
                    );
                },
                create: function(vertex, feature) {
                    this.layer.events.triggerEvent(
                        "sketchstarted", {vertex: vertex, feature: feature}
                    );					
                }
            },
            this.callbacks
        );
        this.layer = layer;
        this.handlerOptions = this.handlerOptions || {};
        if (!("multi" in this.handlerOptions)) {
            this.handlerOptions.multi = this.multi;
        }
        var sketchStyle = this.layer.styleMap && this.layer.styleMap.styles.temporary;
        if(sketchStyle) {
            this.handlerOptions.layerOptions = OpenLayers.Util.applyDefaults(
                this.handlerOptions.layerOptions,
                {styleMap: new OpenLayers.StyleMap({"default": sketchStyle})}
            );
        }
        this.handler = new handler(this, this.callbacks, this.handlerOptions);
    },

    /**
     * Method: drawFeature
     */
    drawFeature: function(geometry) {
        var feature = new OpenLayers.Feature.Vector(geometry);
        var proceed = this.layer.events.triggerEvent(
            "sketchcomplete", {feature: feature}
        );
        if(proceed !== false) {
            feature.state = OpenLayers.State.INSERT;
            this.layer.addFeatures([feature]);
            this.featureAdded(feature);
            this.events.triggerEvent("featureadded",{feature : feature});
			//*****  geometry => tras o valor da posicao do objeto criado.
			//alert(this.handler.title);
			var posicao = geometry;
			var posicao_ponto = geometry;
			//alert(geometry);
			//$("#posicao").val(geometry);
			//$("#posicao_ponto").val(geometry);
			//alert(this.handler.id);
			//alert(posicao_ponto);
			
			var cod = feature.id;
			var identificacao = cod.split("_");
			//$("#identificacao").val(identificacao[1]);
			//alert(posicao);			
			//alert(identificacao[1]);
			
			
			if(this.handler.title == "Ponto"){
			// Adicionar aqui o popup para gravacao na base de dados
				show_form_add(this.layer, feature, '#dialog-ponto', geometry, identificacao[1]);
			}else{
				show_form_add(this.layer, feature, '#dialog-at', geometry, identificacao[1]);
			}
        }
    },

    CLASS_NAME: "OpenLayers.Control.DrawFeature"
});

function salva_at_equipamento(obj) {
	var tipo = "#dialog-at";	
	$.ajax({
		type: 'POST',
		url: obj.action,
		//data: $(obj.name).serialize(),
		data: $("#CadAtEquipamentos").serialize(),		
		beforeSend: function() {
			//$(id_div).html(montaImgLoad(base_url));
		},
		success: function(data){
			alert('dados sendo gravados');
			alert(data);
			//$("#dialog-at").dialog('destroy');
			//$("#dialog-at").hide();			
			// Limpa os campos do formulário
			$('input[name=posicao]').val('');
			$('input[name=nm_serie]').val('');
			$('input[name=modelo]').val('');
			$('input[name=situacao]').val('');
		},
		error: function (data) {
			alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
			//alert(data);
		}
	});

	return false;
}

function salva_ligacao_at(obj) {
	var tipo = "#dialog-at";	
	$.ajax({
		type: 'POST',
		url: obj.action,
		//data: $(obj.name).serialize(),
		data: $("#CadLigacaoAt").serialize(),		
		beforeSend: function() {
			//$(id_div).html(montaImgLoad(base_url));
		},
		success: function(data){
			alert('dados sendo gravados');			
			//$("#tipo_ligacao").val();
			//$("#dialog-at").dialog('destroy');
			//$("#dialog-at").hide();			
			// Limpa os campos do formulário			
		},
		error: function (data) {
			alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
			alert(data);
			//alert(data);
		}
	});

	return false;
}

function salva_ligacao_equipamento_at(obj) {
	var tipo = "#dialog-at";	
	$.ajax({
		type: 'POST',
		url: obj.action,
		//data: $(obj.name).serialize(),
		data: $("#CadLigacaoEquipamentoAt").serialize(),		
		beforeSend: function() {
			//$(id_div).html(montaImgLoad(base_url));
		},
		success: function(data){
			alert('dados sendo gravados');			
			//$("#tipo_ligacao").val();
			//$("#dialog-at").dialog('destroy');
			//$("#dialog-at").hide();			
			// Limpa os campos do formulário			
		},
		error: function (data) {
			alert('Ocorreu um erro inesperado, tente novamente mais tarde!');
			alert(data);
			//alert(data);
		}
	});

	return false;
}
