-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.36-community-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema projeto
--

CREATE DATABASE IF NOT EXISTS projeto;
USE projeto;

--
-- Definition of table `at`
--

DROP TABLE IF EXISTS `at`;
CREATE TABLE `at` (
  `cd_at` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cd_bloco` int(10) unsigned NOT NULL,
  `nm_at` varchar(255) NOT NULL,
  `ds_local` varchar(255) NOT NULL,
  `nr_tamanho` int(10) unsigned NOT NULL,
  `ds_posicao` text NOT NULL,
  `nr_identificacao` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cd_at`),
  KEY `at_FKIndex1` (`cd_bloco`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `at`
--

/*!40000 ALTER TABLE `at` DISABLE KEYS */;
INSERT INTO `at` (`cd_at`,`cd_bloco`,`nm_at`,`ds_local`,`nr_tamanho`,`ds_posicao`,`nr_identificacao`) VALUES 
 (148,1,'AT 01','Sala 09',12,'POLYGON((9178.182337649087 447.4119643957137,9178.182337649087 467.77663969388624,9157.817662350913 467.77663969388624,9157.817662350913 447.4119643957137,9178.182337649087 447.4119643957137))',1124);
/*!40000 ALTER TABLE `at` ENABLE KEYS */;


--
-- Definition of table `at_equipamento`
--

DROP TABLE IF EXISTS `at_equipamento`;
CREATE TABLE `at_equipamento` (
  `cd_at_equipamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cd_equipamento` int(10) unsigned NOT NULL,
  `cd_at` int(10) unsigned NOT NULL,
  `nr_posicao` int(10) unsigned NOT NULL,
  `ds_serie` varchar(255) DEFAULT NULL,
  `in_status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`cd_at_equipamento`),
  KEY `at_equipamento_FKIndex1` (`cd_at`),
  KEY `at_equipamento_FKIndex2` (`cd_equipamento`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `at_equipamento`
--

/*!40000 ALTER TABLE `at_equipamento` DISABLE KEYS */;
INSERT INTO `at_equipamento` (`cd_at_equipamento`,`cd_equipamento`,`cd_at`,`nr_posicao`,`ds_serie`,`in_status`) VALUES 
 (1,1,84,5,'0','A'),
 (2,1,83,0,'986565','A'),
 (3,1,85,455,'45645645645','A'),
 (4,3,86,1,'0','I'),
 (5,2,87,5,'BR102522FFFRF555','A'),
 (6,3,88,1,'','A'),
 (7,1,88,2,'BR125454BR','A'),
 (8,2,88,3,'BRJHJHGJHRG','A'),
 (9,3,89,0,'21313','A'),
 (10,1,90,2,'321321','A'),
 (11,4,91,4,'sdf','A'),
 (12,4,92,4,'sf','A'),
 (13,4,93,5,'jkh','A'),
 (14,1,94,5,'sdfg','A'),
 (15,4,95,8,'kjhkh','A'),
 (16,4,96,9,'LKJ','A'),
 (18,4,98,5,'rg','A'),
 (19,4,100,2,'4','A'),
 (20,4,101,5,'kjh','A'),
 (21,2,102,2,'kshdf','A'),
 (22,2,103,2,'r','A'),
 (23,2,104,5,'dfg','A'),
 (25,2,106,5,'dfg','A'),
 (26,2,107,6,'df','A'),
 (27,2,107,6,'df','A'),
 (28,2,108,2,'dgh','A'),
 (29,2,109,6,'r','A'),
 (30,2,109,6,'r','A'),
 (31,2,109,6,'r','A'),
 (32,2,111,8,'dfg','A'),
 (33,2,112,5,'drt','A'),
 (34,2,113,34,'','A'),
 (36,2,115,5,'fg','A'),
 (37,2,116,5,'dfg','A'),
 (38,2,117,5,'sf','A'),
 (39,2,118,5,'sd','A'),
 (41,2,120,5,'','A'),
 (42,2,121,5,'dfg','A'),
 (43,2,122,5,'drt','A'),
 (44,2,123,5,'B','A'),
 (45,2,125,8,'dfg','A'),
 (46,4,125,10,'df','A'),
 (47,2,125,1,'BRRB987987JH','A'),
 (48,2,125,1,'BRFabio','A'),
 (50,3,139,10,'BR123654','A'),
 (76,2,148,2,'','A'),
 (72,2,148,7,'','A'),
 (73,2,148,10,'','A'),
 (56,4,0,3,'','A'),
 (74,3,148,3,'','A'),
 (75,3,148,9,'','A');
/*!40000 ALTER TABLE `at_equipamento` ENABLE KEYS */;


--
-- Definition of table `at_equipamento_porta`
--

DROP TABLE IF EXISTS `at_equipamento_porta`;
CREATE TABLE `at_equipamento_porta` (
  `cd_at_equipamento_porta` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cd_at_equipamento` int(10) unsigned NOT NULL,
  `nr_porta` int(10) unsigned NOT NULL,
  `in_status` enum('A','I','D') NOT NULL DEFAULT 'A',
  `is_certificado` enum('S','N') NOT NULL DEFAULT 'S',
  PRIMARY KEY (`cd_at_equipamento_porta`),
  KEY `at_equipamento_porta_FKIndex1` (`cd_at_equipamento`)
) ENGINE=MyISAM AUTO_INCREMENT=748 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `at_equipamento_porta`
--

/*!40000 ALTER TABLE `at_equipamento_porta` DISABLE KEYS */;
INSERT INTO `at_equipamento_porta` (`cd_at_equipamento_porta`,`cd_at_equipamento`,`nr_porta`,`in_status`,`is_certificado`) VALUES 
 (1,45,1,'A','N'),
 (2,45,2,'I','N'),
 (3,45,3,'D','N'),
 (4,45,4,'I','N'),
 (5,45,5,'A','N'),
 (6,45,6,'D','N'),
 (7,45,7,'I','N'),
 (8,45,8,'A','N'),
 (9,45,9,'D','N'),
 (10,45,10,'I','N'),
 (11,45,11,'A','N'),
 (12,45,12,'D','N'),
 (13,45,13,'A','N'),
 (14,45,14,'I','N'),
 (15,45,15,'D','N'),
 (16,45,16,'I','N'),
 (17,45,17,'A','N'),
 (18,45,18,'D','N'),
 (19,45,19,'I','N'),
 (20,45,20,'A','N'),
 (21,45,21,'D','N'),
 (22,45,22,'I','N'),
 (23,45,23,'A','N'),
 (24,45,24,'D','N'),
 (25,46,1,'I','N'),
 (26,46,2,'I','N'),
 (27,46,3,'I','N'),
 (28,46,4,'I','N'),
 (29,46,5,'I','N'),
 (30,46,6,'I','N'),
 (31,46,7,'I','N'),
 (32,46,8,'I','N'),
 (33,46,9,'I','N'),
 (34,46,10,'I','N'),
 (35,46,11,'I','N'),
 (36,46,12,'I','N'),
 (37,46,13,'I','N'),
 (38,46,14,'I','N'),
 (39,46,15,'I','N'),
 (40,46,16,'I','N'),
 (41,46,17,'I','N'),
 (42,46,18,'I','N'),
 (43,46,19,'I','N'),
 (44,46,20,'I','N'),
 (45,46,21,'I','N'),
 (46,46,22,'I','N'),
 (47,46,23,'I','N'),
 (48,46,24,'I','N'),
 (49,47,1,'I','N'),
 (50,47,2,'I','N'),
 (51,47,3,'I','N'),
 (52,47,4,'I','N'),
 (53,47,5,'I','N'),
 (54,47,6,'I','N'),
 (55,47,7,'I','N'),
 (56,47,8,'I','N'),
 (57,47,9,'I','N'),
 (58,47,10,'I','N'),
 (59,47,11,'I','N'),
 (60,47,12,'I','N'),
 (61,47,13,'I','N'),
 (62,47,14,'I','N'),
 (63,47,15,'I','N'),
 (64,47,16,'I','N'),
 (65,47,17,'I','N'),
 (66,47,18,'I','N'),
 (67,47,19,'I','N'),
 (68,47,20,'I','N'),
 (69,47,21,'I','N'),
 (70,47,22,'I','N'),
 (71,47,23,'I','N'),
 (72,47,24,'I','N'),
 (73,48,1,'I','N'),
 (74,48,2,'I','N'),
 (75,48,3,'I','N'),
 (76,48,4,'I','N'),
 (77,48,5,'I','N'),
 (78,48,6,'I','N'),
 (79,48,7,'I','N'),
 (80,48,8,'I','N'),
 (81,48,9,'I','N'),
 (82,48,10,'I','N'),
 (83,48,11,'I','N'),
 (84,48,12,'I','N'),
 (85,48,13,'I','N'),
 (86,48,14,'I','N'),
 (87,48,15,'I','N'),
 (88,48,16,'I','N'),
 (89,48,17,'I','N'),
 (90,48,18,'I','N'),
 (91,48,19,'I','N'),
 (92,48,20,'I','N'),
 (93,48,21,'I','N'),
 (94,48,22,'I','N'),
 (95,48,23,'I','N'),
 (96,48,24,'I','N'),
 (121,50,1,'I','N'),
 (122,50,2,'I','N'),
 (123,50,3,'I','N'),
 (124,50,4,'I','N'),
 (125,50,5,'I','N'),
 (126,50,6,'I','N'),
 (127,50,7,'I','N'),
 (128,50,8,'I','N'),
 (129,50,9,'I','N'),
 (130,50,10,'I','N'),
 (131,50,11,'I','N'),
 (132,50,12,'I','N'),
 (133,50,13,'I','N'),
 (134,50,14,'I','N'),
 (135,50,15,'I','N'),
 (136,50,16,'I','N'),
 (137,50,17,'I','N'),
 (138,50,18,'I','N'),
 (139,50,19,'I','N'),
 (140,50,20,'I','N'),
 (141,50,21,'I','N'),
 (142,50,22,'I','N'),
 (143,50,23,'I','N'),
 (144,50,24,'I','N'),
 (675,73,24,'I','S'),
 (674,73,23,'I','S'),
 (673,73,22,'I','S'),
 (672,73,21,'I','S'),
 (671,73,20,'I','S'),
 (670,73,19,'I','S'),
 (669,73,18,'I','S'),
 (668,73,17,'I','S'),
 (667,73,16,'I','S'),
 (666,73,15,'I','S'),
 (665,73,14,'I','S'),
 (664,73,13,'I','S'),
 (663,73,12,'I','S'),
 (662,73,11,'I','S'),
 (661,73,10,'I','S'),
 (660,73,9,'I','S'),
 (659,73,8,'I','S'),
 (658,73,7,'I','S'),
 (657,73,6,'I','S'),
 (656,73,5,'I','S'),
 (655,73,4,'I','S'),
 (654,73,3,'I','S'),
 (653,73,2,'I','S'),
 (652,73,1,'I','S'),
 (747,76,24,'I','S'),
 (746,76,23,'I','S'),
 (745,76,22,'I','S'),
 (744,76,21,'I','S'),
 (743,76,20,'I','S'),
 (742,76,19,'I','S'),
 (741,76,18,'I','S'),
 (740,76,17,'I','S'),
 (739,76,16,'I','S'),
 (738,76,15,'I','S'),
 (737,76,14,'I','S'),
 (736,76,13,'I','S'),
 (735,76,12,'I','S'),
 (734,76,11,'I','S'),
 (733,76,10,'I','S'),
 (732,76,9,'I','S'),
 (731,76,8,'I','S'),
 (730,76,7,'I','S'),
 (729,76,6,'I','S'),
 (728,76,5,'I','S'),
 (727,76,4,'I','S'),
 (726,76,3,'I','S'),
 (725,76,2,'I','S'),
 (724,76,1,'I','S'),
 (244,56,1,'I','N'),
 (245,56,2,'I','N'),
 (246,56,3,'I','N'),
 (247,56,4,'I','N'),
 (248,56,5,'I','N'),
 (249,56,6,'I','N'),
 (250,56,7,'I','N'),
 (251,56,8,'I','N'),
 (252,56,9,'I','N'),
 (253,56,10,'I','N'),
 (254,56,11,'I','N'),
 (255,56,12,'I','N'),
 (256,56,13,'I','N'),
 (257,56,14,'I','N'),
 (258,56,15,'I','N'),
 (259,56,16,'I','N'),
 (260,56,17,'I','N'),
 (261,56,18,'I','N'),
 (262,56,19,'I','N'),
 (263,56,20,'I','N'),
 (264,56,21,'I','N'),
 (265,56,22,'I','N'),
 (266,56,23,'I','N'),
 (267,56,24,'I','N'),
 (699,74,24,'I','S'),
 (698,74,23,'I','S'),
 (697,74,22,'I','S'),
 (696,74,21,'I','S'),
 (695,74,20,'I','S'),
 (694,74,19,'I','S'),
 (693,74,18,'I','S'),
 (692,74,17,'I','S'),
 (691,74,16,'I','S'),
 (690,74,15,'I','S'),
 (689,74,14,'I','S'),
 (688,74,13,'A','S'),
 (687,74,12,'I','S'),
 (686,74,11,'I','S'),
 (685,74,10,'I','S'),
 (684,74,9,'I','S'),
 (683,74,8,'I','S'),
 (682,74,7,'I','S'),
 (681,74,6,'I','S'),
 (680,74,5,'A','S'),
 (679,74,4,'I','S'),
 (678,74,3,'A','S'),
 (677,74,2,'A','S'),
 (676,74,1,'I','S'),
 (723,75,24,'A','S'),
 (722,75,23,'A','S'),
 (721,75,22,'A','S'),
 (720,75,21,'A','S'),
 (719,75,20,'A','S'),
 (718,75,19,'A','S'),
 (717,75,18,'A','S'),
 (716,75,17,'A','S'),
 (715,75,16,'A','S'),
 (714,75,15,'A','S'),
 (713,75,14,'A','S'),
 (712,75,13,'A','S'),
 (711,75,12,'A','S'),
 (710,75,11,'A','S'),
 (709,75,10,'A','S'),
 (708,75,9,'A','S'),
 (707,75,8,'A','S'),
 (706,75,7,'A','S'),
 (705,75,6,'A','S'),
 (704,75,5,'A','S'),
 (703,75,4,'A','S'),
 (702,75,3,'A','S'),
 (701,75,2,'A','S'),
 (700,75,1,'A','S'),
 (651,72,24,'I','S'),
 (650,72,23,'I','S'),
 (649,72,22,'I','S'),
 (648,72,21,'I','S'),
 (647,72,20,'I','S'),
 (646,72,19,'I','S'),
 (645,72,18,'I','S'),
 (644,72,17,'I','S'),
 (643,72,16,'I','S'),
 (642,72,15,'I','S'),
 (641,72,14,'I','S'),
 (640,72,13,'I','S'),
 (639,72,12,'I','S'),
 (638,72,11,'I','S'),
 (637,72,10,'I','S'),
 (636,72,9,'I','S'),
 (635,72,8,'I','S'),
 (634,72,7,'I','S'),
 (633,72,6,'I','S'),
 (632,72,5,'I','S'),
 (631,72,4,'I','S'),
 (630,72,3,'I','S'),
 (629,72,2,'I','S'),
 (628,72,1,'I','S');
/*!40000 ALTER TABLE `at_equipamento_porta` ENABLE KEYS */;


--
-- Definition of table `bloco`
--

DROP TABLE IF EXISTS `bloco`;
CREATE TABLE `bloco` (
  `cd_bloco` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nm_bloco` varchar(255) NOT NULL,
  PRIMARY KEY (`cd_bloco`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloco`
--

/*!40000 ALTER TABLE `bloco` DISABLE KEYS */;
INSERT INTO `bloco` (`cd_bloco`,`nm_bloco`) VALUES 
 (1,'Bloco Administrativo'),
 (2,'Bloco da  Biblioteca'),
 (3,'Bloco XXI-A'),
 (4,'Bloco XXI-B'),
 (5,'Bloco XXI-C'),
 (6,'Bloco P - P?s e Extens?o'),
 (7,'Bloco A'),
 (8,'Bloco Z'),
 (9,'Bloco DCE'),
 (10,'Banco Santander'),
 (11,'Centro de Eventos'),
 (12,'CAP/Salas Ed. F?sica'),
 (13,'Piscina'),
 (14,'Sala de Dan?a/Academia'),
 (15,'Bloco da Sa?de'),
 (16,'Clinicas'),
 (17,'Sede da Aprofucri'),
 (18,'Horto Florestal/Pega'),
 (19,'Bloco T'),
 (20,'Cantina Universidade'),
 (21,'Terminal de ?nibus'),
 (22,'Cantina Universitaria'),
 (23,'Audit?rio Hulse'),
 (24,'Lanchonete Doce P?o'),
 (25,'Bloco B'),
 (26,'Bloco C'),
 (27,'Bloco D'),
 (28,'Bloco E'),
 (29,'Bloco F'),
 (30,'Bloco G'),
 (31,'Bloco H'),
 (32,'Bloco I'),
 (33,'Bloco J'),
 (34,'Bloco K'),
 (35,'Bloco L'),
 (36,'Bloco M'),
 (37,'Bloco N'),
 (38,'Bloco O'),
 (39,'Bloco Q'),
 (40,'Bloco R'),
 (41,'Bloco S'),
 (42,'Bloco U'),
 (43,'Bloco V'),
 (44,'Bloco X'),
 (45,'Bloco W'),
 (46,'Bloco Y'),
 (47,'Bloco Centac'),
 (48,'Bloco Apoio'),
 (49,'IPAT'),
 (50,'Bloco do Estudante');
/*!40000 ALTER TABLE `bloco` ENABLE KEYS */;


--
-- Definition of table `equipamento`
--

DROP TABLE IF EXISTS `equipamento`;
CREATE TABLE `equipamento` (
  `cd_equipamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nm_equipamento` varchar(255) NOT NULL,
  `ds_modelo` varchar(150) NOT NULL,
  `nr_porta` int(10) unsigned NOT NULL,
  `is_gerenciavel` enum('S','N') NOT NULL DEFAULT 'S',
  PRIMARY KEY (`cd_equipamento`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipamento`
--

/*!40000 ALTER TABLE `equipamento` DISABLE KEYS */;
INSERT INTO `equipamento` (`cd_equipamento`,`nm_equipamento`,`ds_modelo`,`nr_porta`,`is_gerenciavel`) VALUES 
 (1,'Roteador','D-link',3,'S'),
 (2,'Switch','Planet',24,'N'),
 (3,'Patch Panel','Yorker',24,'N'),
 (4,'Hub','Básico',24,'N');
/*!40000 ALTER TABLE `equipamento` ENABLE KEYS */;


--
-- Definition of table `ponto`
--

DROP TABLE IF EXISTS `ponto`;
CREATE TABLE `ponto` (
  `cd_ponto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cd_bloco` int(10) unsigned NOT NULL,
  `cd_at_equipamento_porta` int(10) unsigned NOT NULL,
  `cd_tipo_ponto` int(10) unsigned NOT NULL,
  `nm_ponto` varchar(255) NOT NULL,
  `ds_local` varchar(255) NOT NULL,
  `ds_posicao` text NOT NULL,
  `in_status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`cd_ponto`),
  KEY `ponto_FKIndex1` (`cd_tipo_ponto`),
  KEY `ponto_FKIndex2` (`cd_at_equipamento_porta`),
  KEY `ponto_FKIndex3` (`cd_bloco`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ponto`
--

/*!40000 ALTER TABLE `ponto` DISABLE KEYS */;
INSERT INTO `ponto` (`cd_ponto`,`cd_bloco`,`cd_at_equipamento_porta`,`cd_tipo_ponto`,`nm_ponto`,`ds_local`,`ds_posicao`,`in_status`) VALUES 
 (15,1,700,1,'01/01','Sala 01','POLYGON((8905.470765814496 608.1458284905999,8893 629.7458284905999,8880.529234185504 608.1458284905999,8905.470765814496 608.1458284905999))','A'),
 (16,1,701,1,'01/02','Sala 01','POLYGON((8651.470765814496 1070.1458284906,8639 1091.7458284906002,8626.529234185504 1070.1458284906,8651.470765814496 1070.1458284906))','A'),
 (17,1,702,1,'01/03','Sala 03','POLYGON((8903.470765814496 978.1458284905999,8891 999.7458284905999,8878.529234185504 978.1458284905999,8903.470765814496 978.1458284905999))','A'),
 (18,1,703,1,'01/04','Sala 02','POLYGON((7954.855289353496 1000.1458284905999,7942.384523539 1021.7458284905999,7929.913757724504 1000.1458284905999,7954.855289353496 1000.1458284905999))','A'),
 (19,1,705,1,'01/10','Sala 02','POLYGON((7956.976348001696 758.1458284905999,7944.5055821872 779.7458284905999,7932.034816372704 758.1458284905999,7956.976348001696 758.1458284905999))','A'),
 (20,1,704,1,'01/06','Sala 03','POLYGON((7466.974480662696 1074.1458284906,7454.5037148482 1095.7458284906002,7442.0329490337035 1074.1458284906,7466.974480662696 1074.1458284906))','A'),
 (21,1,717,1,'01/08','Sala 03','POLYGON((7412.974480662696 454.1458284906,7400.5037148482 475.7458284906,7388.0329490337035 454.1458284906,7412.974480662696 454.1458284906))','A'),
 (22,1,706,1,'01/07','Sala 04','POLYGON((6966.974480662696 604.1458284905999,6954.5037148482 625.7458284905999,6942.0329490337035 604.1458284905999,6966.974480662696 604.1458284905999))','A'),
 (23,1,707,1,'01/09','Sala 04','POLYGON((7258.974480662696 1000.1458284905999,7246.5037148482 1021.7458284905999,7234.0329490337035 1000.1458284905999,7258.974480662696 1000.1458284905999))','A'),
 (24,1,721,1,'01/11','Sala 05','POLYGON((6670.974480662696 1086.1458284906,6658.5037148482 1107.7458284906002,6646.0329490337035 1086.1458284906,6670.974480662696 1086.1458284906))','A'),
 (25,1,708,1,'01/12','Sala 05','POLYGON((6934.974480662696 986.1458284905999,6922.5037148482 1007.7458284905999,6910.0329490337035 986.1458284905999,6934.974480662696 986.1458284905999))','A'),
 (26,1,709,1,'01/13','Sala 05\'','POLYGON((6642.974480662696 926.1458284905999,6630.5037148482 947.7458284905999,6618.0329490337035 926.1458284905999,6642.974480662696 926.1458284905999))','A'),
 (27,1,718,1,'01/14','Sala 05','POLYGON((6634.974480662696 642.1458284905999,6622.5037148482 663.7458284905999,6610.0329490337035 642.1458284905999,6634.974480662696 642.1458284905999))','A'),
 (28,1,712,1,'01/15','Sala 05','POLYGON((6636.974480662696 573.29781372784,6624.5037148482 594.89781372784,6612.0329490337035 573.29781372784,6636.974480662696 573.29781372784))','A'),
 (29,1,710,1,'01/16','Sala 06','POLYGON((6428.974480662696 1077.2978137277998,6416.5037148482 1098.8978137278,6404.0329490337035 1077.2978137277998,6428.974480662696 1077.2978137277998))','A'),
 (30,1,711,1,'01/17','Sala 06','POLYGON((6508.974480662696 1071.2978137277998,6496.5037148482 1092.8978137278,6484.0329490337035 1071.2978137277998,6508.974480662696 1071.2978137277998))','A'),
 (31,1,713,1,'01/18','Sala 06','POLYGON((6602.974480662696 1027.2978137277998,6590.5037148482 1048.8978137278,6578.0329490337035 1027.2978137277998,6602.974480662696 1027.2978137277998))','A'),
 (32,1,714,1,'01/19','Sala 06','POLYGON((5971.909148916996 1039.2978137279,5959.4383831025 1060.8978137279,5946.967617288004 1039.2978137279,5971.909148916996 1039.2978137279))','A'),
 (33,1,715,1,'01/20','Sala 06','POLYGON((6087.909148916996 457.29781372786,6075.4383831025 478.89781372785995,6062.967617288004 457.29781372786,6087.909148916996 457.29781372786))','A'),
 (34,1,716,1,'01/21','Sala 06','POLYGON((6403.909148916996 451.29781372786,6391.4383831025 472.89781372785995,6378.967617288004 451.29781372786,6403.909148916996 451.29781372786))','A'),
 (35,1,719,1,'01/22','Sala 06','POLYGON((5962.121982735296 731.29781372786,5949.6512169208 752.89781372786,5937.180451106304 731.29781372786,5962.121982735296 731.29781372786))','A'),
 (36,1,720,1,'01/23','Sala 06','POLYGON((5964.121982735296 933.29781372786,5951.6512169208 954.89781372786,5939.180451106304 933.29781372786,5964.121982735296 933.29781372786))','A'),
 (37,1,722,1,'01/23','Sala 06','POLYGON((6608.088379565496 605.29781372782,6595.617613751 626.89781372782,6583.146847936504 605.29781372782,6608.088379565496 605.29781372782))','A'),
 (38,1,723,1,'01/24','Sala 06','POLYGON((5964.088379565496 563.29781372782,5951.617613751 584.89781372782,5939.146847936504 563.29781372782,5964.088379565496 563.29781372782))','A'),
 (39,1,680,1,'01/25','Sala 07','POLYGON((4504.640515325997 1049.2978137279,4492.1697495115 1070.8978137279,4479.698983697004 1049.2978137279,4504.640515325997 1049.2978137279))','A'),
 (40,1,678,1,'01/26','Sala 08','POLYGON((3800.747653164896 1067.2978137279,3788.2768873504 1088.8978137279,3775.8061215359044 1067.2978137279,3800.747653164896 1067.2978137279))','A'),
 (41,1,677,1,'01/27','Sala 08','POLYGON((3374.747653164896 951.2978137279,3362.2768873504 972.8978137279,3349.8061215359044 951.2978137279,3374.747653164896 951.2978137279))','A'),
 (42,1,688,1,'01/28','Sala 08','POLYGON((3880.747653164896 461.2978137279,3868.2768873504 482.8978137279,3855.8061215359044 461.2978137279,3880.747653164896 461.2978137279))','A');
/*!40000 ALTER TABLE `ponto` ENABLE KEYS */;


--
-- Definition of table `tipo_cabeamento`
--

DROP TABLE IF EXISTS `tipo_cabeamento`;
CREATE TABLE `tipo_cabeamento` (
  `cd_tipo_cabeamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nm_tipo_cabeamento` varchar(255) NOT NULL,
  PRIMARY KEY (`cd_tipo_cabeamento`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_cabeamento`
--

/*!40000 ALTER TABLE `tipo_cabeamento` DISABLE KEYS */;
INSERT INTO `tipo_cabeamento` (`cd_tipo_cabeamento`,`nm_tipo_cabeamento`) VALUES 
 (1,'Par tran?ado 10Mbps'),
 (2,'Par tran?ado 100Mbps'),
 (3,'Par tran?ado 1000Mbps');
/*!40000 ALTER TABLE `tipo_cabeamento` ENABLE KEYS */;


--
-- Definition of table `tipo_cabeamento_equipamento`
--

DROP TABLE IF EXISTS `tipo_cabeamento_equipamento`;
CREATE TABLE `tipo_cabeamento_equipamento` (
  `cd_equipamento` int(10) unsigned NOT NULL,
  `cd_tipo_cabeamento` int(10) unsigned NOT NULL,
  KEY `tipo_cabeamento_equipamento_FKIndex1` (`cd_tipo_cabeamento`),
  KEY `tipo_cabeamento_equipamento_FKIndex2` (`cd_equipamento`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_cabeamento_equipamento`
--

/*!40000 ALTER TABLE `tipo_cabeamento_equipamento` DISABLE KEYS */;
INSERT INTO `tipo_cabeamento_equipamento` (`cd_equipamento`,`cd_tipo_cabeamento`) VALUES 
 (1,3),
 (2,3),
 (2,1),
 (2,2),
 (4,1);
/*!40000 ALTER TABLE `tipo_cabeamento_equipamento` ENABLE KEYS */;


--
-- Definition of table `tipo_ponto`
--

DROP TABLE IF EXISTS `tipo_ponto`;
CREATE TABLE `tipo_ponto` (
  `cd_tipo_ponto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nm_tipo_ponto` varchar(100) NOT NULL,
  PRIMARY KEY (`cd_tipo_ponto`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_ponto`
--

/*!40000 ALTER TABLE `tipo_ponto` DISABLE KEYS */;
INSERT INTO `tipo_ponto` (`cd_tipo_ponto`,`nm_tipo_ponto`) VALUES 
 (1,'Rede'),
 (2,'Telefone');
/*!40000 ALTER TABLE `tipo_ponto` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
